# CHANGELOG

This is the Changelog for APE, detailing major bugfixes encountered during development and other changes, such as the addition/removal of features.

### Summary

   - **v0.3** Reimplementation to remove a lot of complex code. Functionally
             equivalent to v0.2.2, modulo any bugfixes.
   - **v0.2.2** Reward poachers when capturing a prey only, minor bugfixes,
             observation space now contains remaining time.
   - **v0.2.1** OrderedDicts for the observation spaces and added action masks 
             to observation_spaces.
   - **v0.2** Re-implements env to use the developed model specification.
   - **v0.1** Builds from implementation 1.2 of the PoacherGame.
   - **v0.0** Initial version, unstable.


## v0.3

This implements a partial rewrite of the original Anti-Poaching game to remove dependencies on custom wrappers. This resulted in a lot of simplifications in other areas of the code as well. Additional features include:

    - Added GridStateVaryingPCA which allows to vary the prob-anim per cell (modes: kernel/random)
    - Fixed the seed behaviour. Seeded environments will now start from the same position and the same environment, but the seeds used for each agent's obs and action spaces are now distinct (but deterministic, once the initial seed is given).

## v0.2.2

- Added the RGB render mode using pygame.

- Bugfixes
    
    - Previously, in the `_poachers_move_and_get_traps` method, the main method only considered poachers who moved i.e. actions 1 to 4. This includes processing Poachers who recover their traps. Effectively, Poachers who did not move did not recover their traps. We now consider poachers whose actions are 0 to 4.

    - In the `utils/game_utils.py` file, in the `GridState` class, the method `add_trap` previously assigned the position object as the Trap location. However, Python here creates a reference to the location. So, the trap location would be a shallow copy of the Poacher position i.e. we have tracking Traps !!! This has since been corrected to a copy of the position, and we do not have this bug.

## v0.2.1

- Introduces minor changes to the Observation Spaces of each agent.

    - Action masks are now included with the observation spaces. This reflects the return values of the `ConservationGame.observe` method, and general practice with ParallelEnv implementations.

    - The `observation_spaces[agent]["observation"]` Dictionaries, i.e. the actual observation space of each agent, is implemented as an `collections.OrderedDict` object. This addresses a bug in the `box_flatten` method, where the flattening was taken according to the alphabetic ordering of the keys. To preserve this order, the spaces needed to be created with an order, thus the change in the main source.

- The `NonCategoricalFlatten` wrapper is designed to do all it can to convert a complex `gymnasium.spaces.Space` into a `Box`, at least for `Discrete`, `MultiDiscrete`, `Dict`, `Binary` and `MultiBinary` types. This ensures the following.

    - There is compatibility with the `ActionMaskModel`, and the general `MultiAgentEnv` class from RLlib (v2.4.0).

    - The `action_mask` keys are converted as well due to a well-known deserialisation bug with `MultiBinary` in v2.4.0 of Ray. Blocking serialisation means that the checkpointing capabilities of Ray are broken. Upgrading to later versions (v2.5.0+) resolves the bug, but forces us to adopt breaking changes in the API and use cases. Furthermore, converting to a `Box` type only changes the type and not the space behaviour or its elements.

## v0.2.0 

This is the implmentation according to the formal specification.

### Bugfixes 

- observation spaces did not contain the observations generated during program execution. This was due to 

    - Poachers can have negative location due to `GridState.NULL_POS`, and therefore, `MultiDiscrete([n,n])` cannot allow this. Therefore, this has been replaced by a `Box( ... , dtype=np.int8)` to allow for this. 
    - The `Discrete` and `MultiDiscrete` spaces represent discrete $[a,b)$ spaces, which did not allow certain observations such as detecting all possible agents in the cell. While pathological, this is still a valid case.
    - `GridState.NULL_POS` was implicitly a `np.float32` array instead of the more appropriate `np.int8`. This caused the spaces to falsely reject valid samples.

- `place_trap` was not executing.

    - Problem with checking and setting the wrong indices/actions/flags. It is working now.
