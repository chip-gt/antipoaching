"""Module that implements the wrappers to make the Anti-Poaching game
compatible with RLlib."""

import numpy as np

# PettingZoo imports
from gymnasium.core import ObservationWrapper
from gymnasium import spaces as gsp
from pettingzoo.utils.env import ParallelEnv

# Ray imports
from ray.rllib.env.multi_agent_env import MultiAgentEnv
from .typing import INTEGER


class QMIXCompatibilityLayer(MultiAgentEnv):
    """Class that renames the "observations" to "obs". This is for
    compatibility with QMIX."""

    def __init__(self, env):
        super().__init__()
        self.env = env
        self._obs_space_in_preferred_format = True
        self.observation_space = {
            agent: gsp.Dict(
                {
                    "action_mask": env.observation_space(agent)["action_mask"],
                    "obs": env.observation_space(agent)["observations"],
                }
            )
            for agent in env.agents
        }

        self._action_space_in_preferred_format = True
        self.action_space = env.action_space

    def observation(self, obs: dict):
        """Applies the modification to all observations sent out by step
        and reset i.e.
            env.step -> observation(obs), rewards, ...
            env.reset-> observation(obs), info
        """
        for agent in self.observation_space.keys():
            obs[agent]["obs"] = obs[agent].pop("observations")
        return obs

    def step(self, action_dict: dict) -> tuple:
        """Pads each returned dictionary to contain dead agents as well"""
        obs, rewards, terms, truncs, infos = self.env.step(action_dict)

        # RLlib-specific keys
        terms["__all__"] = all(terms.values())
        truncs["__all__"] = all(truncs.values())

        return self.observation(obs), rewards, terms, truncs, infos

    def reset(self, *, seed=None, options=None) -> tuple:
        """Modifies the :attr:`env` after calling :meth:`reset`,
        returning a modified observation using :meth:`self.observation`."""
        reset_obs, infos = self.env.reset(seed=seed, options=options)
        return self.observation(reset_obs), infos

    @property
    def get_sub_environments(self):
        """Returns the unwrapped environment"""
        _unwrapped = getattr(self.env, "unwrapped", self.env)
        if callable(_unwrapped):
            return _unwrapped()
        return _unwrapped


class FullStateObservationWrapper(ObservationWrapper):
    """Changes the observation of each agent to the full
    state of the current game."""

    def __init__(self, env: ParallelEnv):
        super().__init__(env)
        env = self.unwrapped  # Get the base env
        grid_size, ntraps = env.grid.N, env.ntraps_per_poacher
        self.ptraps = [
            _ptrap
            for _ptraps in env.poacher_traps.values()
            for _ptrap in _ptraps
        ]
        self.single_observation_space = gsp.Dict(
            {
                **{
                    poacher: gsp.Box(
                        low=np.array([-1, -1, 0, 0]),
                        high=np.array(
                            [grid_size] * 2 + [ntraps, np.iinfo(INTEGER).max]
                        ),
                        dtype=INTEGER,
                    )
                    for poacher in env.poachers
                },
                **{
                    ranger: gsp.Box(
                        low=np.array([0, 0]),
                        high=np.array([grid_size, grid_size]),
                        dtype=INTEGER,
                    )
                    for ranger in env.rangers
                },
                **{
                    trap.name: gsp.Box(
                        low=np.array([-1, -1]),
                        high=np.array([grid_size, grid_size]),
                        dtype=INTEGER,
                    )
                    for trap in self.ptraps
                },
            }
        )
        self.observation_space = {
            agent: self.single_observation_space
            for agent in env.rangers + env.poachers
        }

    def observation(self, observation):
        new_obs = self.unwrapped.grid.state
        # Add null trap statuses (statii?) if needed
        for trap in self.unwrapped.poacher_traps:
            if trap not in new_obs:
                new_obs[trap.name] = -1 * np.ones(2)

        return {
            agent: new_obs
            for agent in self.unwrapped.rangers + self.unwrapped.poachers
        }
