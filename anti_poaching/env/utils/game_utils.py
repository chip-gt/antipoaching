""" Utils for the environment ConservationGame. 
Defines the GridEnv and the agent dataclasses.
"""

from abc import ABC, abstractmethod
from typing_extensions import override
from copy import deepcopy
from dataclasses import dataclass
import numpy as np
import pygame

from .typing import *

FPS = 10
BLK_SIZE = 30

RED = (200, 0, 0)
BLUE = (0, 0, 200)
GREY = (169, 169, 169) 
WHITE = (255, 255, 255) 
YELLOW = (255, 255, 204)
BG_COLOR = (85, 107, 47)


@dataclass(eq=True)
class Trap:
    """Dataclass to support the traps a poacher
    can place, as well as the environment
    can detect. Its value is non-zero if
    an animal has been caught in it."""

    name: str = "Trap"
    value: float = 0

    def __hash__(self):
        """Overriding hash to be hash of Trap.AntiPoachingGame"""
        return hash(self.name)

    def __repr__(self):
        """Overriding the representation for better printing"""
        return self.name


class BaseGridState(ABC):
    """AbstractClass to represent game state of an Anti-Poaching Game."""

    REWARD_MAP: dict = {"TRAP_FOUND": 2, "PREY_FOUND": 2, "POACHER_FOUND": 100}
    NULL_POS: np.array = np.array([-1, -1, 0, 0], dtype=INTEGER)
    RENDER_MODES = ["ansi", "rgb"]

    def __init__(
        self,
        grid_size: int,
        rangers: list,
        poachers: list,
        ntraps_per_poacher: int,
        seed: int = None,
        render_mode: str = "ansi",
    ):
        self.N = grid_size
        self.rng = np.random.default_rng(seed=seed)

        # Store these for reset later
        self.__init_rangers = rangers[:]
        self.__init_poachers = poachers[:]
        self.__init_ntraps_per_poacher = ntraps_per_poacher

        # finally reset
        self.__reinit_state(rangers, poachers, ntraps_per_poacher)

    @abstractmethod
    def permitted_movements(self, agent: str) -> np.array:
        """For an agent, return the permitted actions she can
        take in her current cell. Returns an numpy.array that
        represents an action mask."""
        raise NotImplementedError

    @abstractmethod
    def update_position(self, agent: str, action: int) -> None:
        """Updates the position of an agent in the grid,
        and updates the internal current positions list."""
        raise NotImplementedError

    @abstractmethod
    def valid_pos(self, pos: tuple) -> bool:
        """Checks if a position is valid for the given grid."""
        raise NotImplementedError

    @abstractmethod
    def prob_animal_appear(self, loc: tuple) -> float:
        raise NotImplementedError

    @abstractmethod
    def prob_detect_cell(self, loc: tuple) -> float:
        raise NotImplementedError

    @abstractmethod
    def prob_detect_trap(self, loc: tuple) -> float:
        raise NotImplementedError

    def get_neighbours(self, agent: str) -> tuple:
        """Given an agent, fetch agents in the same cell. If supplied a Trap,
        this will fetch all non-Trap objects (i.e. agents) which are in the
        same cell."""
        agent_pos = self.state[agent][0:2]
        return [
            _a
            for _a in self.state
            if (not isinstance(_a, Trap))
            and all(self.state[_a][0:2] == agent_pos)
        ]

    def add_trap(self, trap: Trap, pos: tuple) -> None:
        """Adds a trap to the supplied position."""
        self.state[trap] = deepcopy(pos)

    def remove_poacher(self, poacher: str) -> float:
        """Tries to remove an agent from the supplied position.
        Returns a positive reward if successful, else throws an exception."""
        assert poacher in self.state, f"Unauthorised remove !!!\n{self}"
        self.state[poacher] = deepcopy(self.NULL_POS)
        return self.REWARD_MAP["POACHER_FOUND"]

    def remove_trap(self, trap: Trap) -> float:
        """Removes a Trap from the grid."""
        assert trap in self.state, f"Unauthorised trap remove !!!\n{self}"
        del self.state[trap]
        return trap.value

    def reset(
        self,
        seed: int = None,
        **options,
    ) -> None:
        """Resets the grid to initial position. This should use the seed
        supplied to the parent anti_poaching.raw_env instance."""
        self.rng = np.random.default_rng(seed + 8 if seed else None)

        # Get options from the kwargs, and reset
        rangers = options.get("rangers", self.__init_rangers)[:]
        poachers = options.get("poachers", self.__init_poachers)[:]
        ntraps_per_poacher = options.get(
            "ntraps_per_poacher", self.__init_ntraps_per_poacher
        )
        self.__reinit_state(rangers, poachers, ntraps_per_poacher)

    def __reinit_state(
        self,
        rangers: list,
        poachers: list,
        ntraps_per_poacher: int,
    ) -> None:
        """Helper function to reinitialise the state using the RNG engine"""
        self.state = {
            **{
                ranger: self.rng.integers(
                    low=0, high=self.N, size=2, dtype=INTEGER
                )
                for ranger in rangers
            },
            **{
                poacher: np.concatenate(
                    (
                        self.rng.integers(
                            low=0, high=self.N, size=2, dtype=INTEGER
                        ),  # random location
                        np.array(
                            [ntraps_per_poacher, 0], dtype=INTEGER
                        ),  # (..., ntraps, npreys)
                    )
                )
                for poacher in poachers
            },
        }

    def __str__(self) -> str:
        """Gets printable version of GridEnv objects"""
        return "\n".join(
            [f"{thing}: {self.state[thing]}" for thing in self.state]
        )

    def render(self):
        """Forwards the render to ansi or rgb render methods"""
        if self.render_mode == "ansi":
            self._ansi_render()
        elif self.render_mode == "rgb":
            self._rgb_render()

    def _rgb_render(self):
        """Rendering using pygame"""
        self.screen.fill(BG_COLOR)
        for x in range(0, self.window_dim, BLK_SIZE):
            for y in range(0, self.window_dim, BLK_SIZE):
                rect = pygame.Rect(x, y, BLK_SIZE, BLK_SIZE)
                pygame.draw.rect(self.screen, WHITE, rect, 1)

        for obj, pos in self.state.items():
            if pos[0] < 0:
                continue
            width = 0
            if isinstance(obj, Trap):
                color = GREY
                width = (1 - int(obj.value)) * 5
            else:
                color = RED if "poacher" in obj else BLUE
            pos = (pos[0:2] + 0.5) * BLK_SIZE
            pygame.draw.circle(self.screen, color, pos, 10, width=width)

        pygame.display.update()
        self.clock.tick(FPS)

    def _ansi_render(self):
        """Rendering as text"""
        print("\nCurrent state:")
        print(f"\tGrid size: {self.N}")

        printgrid = {(i, j): [] for i in range(self.N) for j in range(self.N)}
        for thing, pos in self.state.items():
            if pos[0] < 0:
                continue  # Do nothing for captured agents
            printgrid[tuple(pos[0:2].tolist())].append(thing)

        for i in range(self.N):
            for j in range(self.N):
                print(printgrid[(i, j)], end=" | ")
            print()


class GridStateConstProb(BaseGridState):
    """
     Class to represent the game state for the Anti-Poaching Game.
    State is represented as a dictionary of player-(location-misc)
    pairs.

    v3. Promoted to GridStateConstProb - this implements BaseGridState.

    v2. Deprecates the grid dictionary for simpler code.
        The GridEnv class is now upgraded to the GridState class,
        and stores the complete state, as described in the
        model document.

    v1. First stable version without bugs, but GridEnv maintained both
        a grid and a current positions list.
    """

    def __init__(
        self,
        grid_size: int,
        rangers: list,
        poachers: list,
        ntraps_per_poacher: int,
        prob_detect_cell: float = 0.2,
        prob_animal_appear: float = 0.2,
        prob_detect_trap: float = 0.2,
        seed: int = None,
        render_mode: str = "ansi",
    ):
        super().__init__(
            grid_size, rangers, poachers, ntraps_per_poacher, seed
        )
        self.render_mode = render_mode
        if self.render_mode == "rgb":
            # These only need to be defined for rgb-mode
            self.window_dim = grid_size * BLK_SIZE
            self.screen = pygame.display.set_mode(
                (self.window_dim, self.window_dim)
            )
            self.clock = pygame.time.Clock()

        # Set the internal probabilities
        self._prob_animal_appear = prob_animal_appear
        self._prob_detect_cell = prob_detect_cell
        self._prob_detect_trap = prob_detect_trap

    @override
    def prob_animal_appear(self, loc: tuple) -> float:
        """Return a constant probability of animal appearance"""
        return self._prob_animal_appear

    @override
    def prob_detect_cell(self, loc: tuple) -> float:
        """Return a constant probability of detecting agents"""
        return self._prob_detect_cell

    @override
    def prob_detect_trap(self, loc: tuple) -> float:
        """Return a constant probability of detecting traps"""
        return self._prob_detect_trap

    @override
    def permitted_movements(self, agent: str) -> np.array:
        """For an agent, return the permitted actions she can
        take in her current cell. Returns an numpy.array that
        represents an action mask."""
        size_action_mask = 5 if "ranger" in agent else 6
        action_mask = np.zeros(size_action_mask, dtype=BOOL)
        action_mask[0] = 1  # always validate null-action

        # Find the agent position
        x, y = self.state[agent][0:2]
        if x == -1 or y == -1:
            return action_mask  # If agent is captured, nothing further.

        # calculate permitted actions for "live" agents
        UP = 0 if x <= 0 else 1
        DOWN = 0 if x >= self.N - 1 else 1
        LEFT = 0 if y <= 0 else 1
        RIGHT = 0 if y >= self.N - 1 else 1

        # Only setting movement flags. Further actions
        # are set by the anti_poaching.raw_env instance.
        action_mask[1:5] = [UP, LEFT, DOWN, RIGHT]
        return action_mask

    @override
    def update_position(self, agent: str, action: int) -> None:
        """Updates the position of an agent in the grid,
        and updates the internal current positions list."""
        # calculate the new position
        new_pos = self.state[agent][0:2]
        x, y = new_pos[0], new_pos[1]
        if action == 1:  # UP
            new_pos = (x - 1, y)
        elif action == 2:  # LEFT
            new_pos = (x, y - 1)
        elif action == 3:  # DOWN
            new_pos = (x + 1, y)
        elif action == 4:  # RIGHT
            new_pos = (x, y + 1)

        # update grid and state list
        assert self.valid_pos(
            new_pos
        ), f"update_position({agent}): Invalid action. {(x,y)=} -> {action=} -> {new_pos=}, grid size = {self.N}"
        self.state[agent][0:2] = new_pos

    @override
    def valid_pos(self, pos: tuple) -> bool:
        """Checks if a position is valid for the given grid."""
        return (0 <= pos[0] < self.N) and (0 <= pos[1] < self.N)


class GridStateVaryingPCA(BaseGridState):
    """Class to represent the game state for the Anti-Poaching Game.
    This class allows probabilities for animal capture to change based on grid
    size. Thus, `prob_animal_appear' should be a list of two floats:
        prob_animal_appear = [lower, upper]
    such that
        sum(prob_animal_appear) = 1.0,
        lower >= 0, upper >=0
    i.e. it is a well-defined probability vector. It can further be initialised
    in two modes:
        1) "random" : Each cell is randomly given a probability between `lower`
        and `upper`.
        2) "kernel" : The central cell is given `prob_animal_appear` as
        `upper` which falls gradually to `lower` at the borders of the grid.
    """

    def __init__(
        self,
        grid_size: int,
        rangers: list,
        poachers: list,
        ntraps_per_poacher: int,
        prob_animal_appear: list,
        prob_detect_cell: np.array = 0.2,
        prob_detect_trap: np.array = 0.2,
        seed: int = None,
        render_mode: str = "ansi",
        pca_mode: str = "random",
    ):
        # Check if prob_animal_appear is well-defined.
        assert (
            isinstance(prob_animal_appear, list)
            and len(prob_animal_appear) == 2
        ), f"{prob_animal_appear=} should be list of [prob_lower, prob_upper]"

        super().__init__(
            grid_size, rangers, poachers, ntraps_per_poacher, seed
        )

        # Now initialise the prob_animal_appear and other probabilities
        self._prob_detect_cell = prob_detect_cell
        self._prob_detect_trap = prob_detect_trap
        lower, upper = prob_animal_appear
        if pca_mode == "random":
            self._prob_animal_appear = self.rng.uniform(
                low=lower,
                high=upper,
                size=(grid_size, grid_size),
            )
        elif pca_mode == "kernel":
            self._prob_animal_appear = np.full(
                shape=(grid_size, grid_size), fill_value=lower
            )
            for i in range(grid_size // 2):
                fill_value = [
                    lower + 2 * (i + 1) * (upper - lower) / grid_size
                ] * 4
                (
                    self._prob_animal_appear[i:-i, i],  # Top row
                    self._prob_animal_appear[i:-i, -i - 1],  # Right column
                    self._prob_animal_appear[i, i:-i],  # Bottom row
                    self._prob_animal_appear[-i - 1, i:-i],  # Left column
                ) = fill_value

            if grid_size % 2 == 1:
                # Then the central cell is not touched by above loop
                self._prob_animal_appear[grid_size // 2, grid_size // 2] = (
                    upper
                )
        else:
            raise NotImplementedError(
                f"{pca_mode=} is not supported (Use `random` or `kernel`)"
            )

        self.render_mode = render_mode
        if self.render_mode == "rgb":
            # These only need to be defined for rgb-mode
            self.window_dim = grid_size * BLK_SIZE
            self.screen = pygame.display.set_mode(
                (self.window_dim, self.window_dim)
            )
            pygame.display.set_caption(
                f"APE: VaryingPCA ({prob_animal_appear})"
            )
            self.clock = pygame.time.Clock()

    @override
    def prob_animal_appear(self, loc: tuple) -> float:
        """Return a constant probability of animal appearance"""
        x, y = loc
        return self._prob_animal_appear[x, y]

    @override
    def prob_detect_cell(self, loc: tuple) -> float:
        """Return a constant probability of detecting agents"""
        return self._prob_detect_cell

    @override
    def prob_detect_trap(self, loc: tuple) -> float:
        """Return a constant probability of detecting traps"""
        return self._prob_detect_trap

    @override
    def permitted_movements(self, agent: str) -> np.array:
        """For an agent, return the permitted actions she can
        take in her current cell. Returns an numpy.array that
        represents an action mask."""
        if "ranger" in agent:
            size_action_mask = 5
        elif "poacher" in agent:
            size_action_mask = 6
        action_mask = np.zeros(size_action_mask, dtype=BOOL)
        action_mask[0] = 1  # always validate null-action

        # Find the agent position
        x, y = self.state[agent][0:2]
        if x == -1 or y == -1:
            return action_mask  # If agent is dead

        # calculate permitted actions
        UP = 0 if x <= 0 else 1
        DOWN = 0 if x >= self.N - 1 else 1
        LEFT = 0 if y <= 0 else 1
        RIGHT = 0 if y >= self.N - 1 else 1

        # Only setting movement flags. Further actions
        # are set by the AntiPoachingGame.observe fn.
        action_mask[1:5] = [UP, LEFT, DOWN, RIGHT]
        return action_mask

    @override
    def update_position(self, agent: str, action: int) -> None:
        """Updates the position of an agent in the grid,
        and updates the internal current positions list."""
        # calculate the new position
        new_pos = self.state[agent][0:2]
        x, y = new_pos[0], new_pos[1]
        if action == 1:  # UP
            new_pos = (x - 1, y)
        elif action == 2:  # LEFT
            new_pos = (x, y - 1)
        elif action == 3:  # DOWN
            new_pos = (x + 1, y)
        elif action == 4:  # RIGHT
            new_pos = (x, y + 1)

        # Throw error if invalid position generated
        # since it needed an invalid action
        assert self.valid_pos(
            new_pos
        ), f"update_position({agent}): Invalid action. {(x,y)=} -> {action=} -> {new_pos=}"

        # update grid and state list
        self.state[agent][0:2] = new_pos

    @override
    def valid_pos(self, pos: tuple) -> bool:
        """Checks if a position is valid for the given grid."""
        return (0 <= pos[0] < self.N) and (0 <= pos[1] < self.N)

    @override
    def _rgb_render(self):
        """Rendering using pygame. This override changes the background color of
        each cell to indicate changing prob_animal_appear or p_CA."""

        # Draw the background
        for i, x in enumerate(range(0, self.window_dim, BLK_SIZE)):
            for j, y in enumerate(range(0, self.window_dim, BLK_SIZE)):
                # Create the rectangle and its surface
                rect = pygame.Rect(x, y, BLK_SIZE, BLK_SIZE)
                surface = pygame.Surface((BLK_SIZE - 1, BLK_SIZE - 1))

                # Fill the surface using self.prob_animal_appear
                color = [
                    int(
                        YELLOW[idx]
                        + (BG_COLOR[idx] - YELLOW[idx])
                        * self.prob_animal_appear((i, j))
                        * 2
                    )
                    for idx in range(3)
                ]
                surface.fill(color)

                # Draw rectangle and blit the surface onto it.
                pygame.draw.rect(self.screen, WHITE, rect, 1)
                self.screen.blit(surface, rect)

        # Now draw the 'live' objects on the grid.
        for obj, pos in self.state.items():
            if pos[0] < 0:
                continue  # Captured poachers aren't drawn.
            width = 0
            if isinstance(obj, Trap):
                color = GREY
                width = (1 - int(obj.value)) * 5
            else:
                color = RED if "poacher" in obj else BLUE
            pos = (pos[0:2] + 0.5) * BLK_SIZE
            pygame.draw.circle(self.screen, color, pos, 10, width=width)

        pygame.display.update()
        self.clock.tick(FPS)
