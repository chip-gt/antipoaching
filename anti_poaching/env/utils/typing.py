"""Module to standardise custom types used for the env and other scripts"""

import numpy as np

INTEGER = np.int32  # Core integer type
BOOL = np.int8  # Action-mask types
AgentID = str  # Agents are strings
