"""Module to expose the Anti-Poaching PettingZoo env. This also registers 
compatibile versions for use with RLlib."""

from gymnasium.spaces import Tuple, Dict, Box
from pettingzoo.utils.env import ParallelEnv

# To define the RLlib environments and register them
from ray.rllib.env.multi_agent_env import MultiAgentEnv
from ray.tune.registry import register_env
from ray.rllib.models import ModelCatalog
from ray.rllib.examples.models.action_mask_model import TorchActionMaskModel
from ray.rllib.env.wrappers.group_agents_wrapper import GroupAgentsWrapper
from ray.rllib.env.wrappers.pettingzoo_env import ParallelPettingZooEnv

from .env import anti_poaching
from .env.utils.game_utils import GridStateConstProb, GridStateVaryingPCA, Trap
from .env.utils.wrappers import (
    QMIXCompatibilityLayer,
    FullStateObservationWrapper,
)


def grouped_rllib_cons_game(
    env: ParallelEnv = None,
    config: dict = None,
) -> GroupAgentsWrapper:
    """This is for QMix and other algorithms that uses agent groups"""
    config = {} if not config else config
    env = anti_poaching.parallel_env(**config) if not env else env
    qenv = QMIXCompatibilityLayer(env)
    grouped_policies = ["rangers", "poachers"]

    # Declare the observation policy of the group to train,
    # and add attr to bypass the QMixPolicy validation step
    obs_space = Tuple(
        Tuple([qenv.observation_space[agent] for agent in getattr(env, group)])
        for group in grouped_policies
    )

    # Declare the grouped action space as well.
    act_space = Tuple(
        Tuple([qenv.action_space(agent) for agent in getattr(env, group)])
        for group in grouped_policies
    )

    # return the grouped env using .with_agent_groups
    return qenv.with_agent_groups(
        groups={group: getattr(env, group) for group in grouped_policies},
        obs_space=obs_space,
        act_space=act_space,
    )


# Register canonical environment to use for RLlib.
register_env(
    anti_poaching.metadata["name"],
    lambda config: ParallelPettingZooEnv(anti_poaching.parallel_env(**config)),
)

# Register Model with full observability to use for RLlib.
register_env(
    anti_poaching.metadata["name"] + "_FullStateObs",
    lambda config: ParallelPettingZooEnv(
        FullStateObservationWrapper(anti_poaching.parallel_env(**config))
    ),
)

# Register grouped environment for QMix
# Note that QMix assumes homogenous agents (ray2.8.0)
register_env(
    anti_poaching.metadata["name"] + "_grouped",
    lambda config: grouped_rllib_cons_game(config=config),
)

# Register action masking model used for all RLlib examples
ModelCatalog.register_custom_model(
    "TorchActionMaskModel", TorchActionMaskModel
)
