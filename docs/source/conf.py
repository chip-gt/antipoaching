# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys

sys.path.insert(0, os.path.abspath("../../examples/manual_policies"))
sys.path.insert(0, os.path.abspath("../../examples/rllib"))
sys.path.insert(0, os.path.abspath("../../examples"))
sys.path.insert(0, os.path.abspath("../../anti_poaching/env/utils"))
sys.path.insert(0, os.path.abspath("../../anti_poaching/env"))
sys.path.insert(0, os.path.abspath("../../anti_poaching"))
sys.path.insert(0, os.path.abspath("../.."))


# -- Project information -----------------------------------------------------

project = "Anti-Poaching"
author = "Maddila Siva Sri Prasanna, Régis Sabbadin, Meritxell Vinyals"
copyright = "2024, " + author

# The full version, including alpha/beta/rc tags
release = "0.3"


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.duration",
    "sphinx.ext.autodoc",
    "sphinx.ext.linkcode",
    "myst_parser",
    "sphinx.ext.autosectionlabel",
    "sphinx.ext.viewcode",
    # "sphinx_multiversion",
]
myst_enable_extensions = ["dollarmath", "amsmath"]
autodoc_mock_imports = ["torch"]


def linkcode_resolve(domain, info):
    if domain != "py":
        return None
    if not info["module"]:
        return None
    filename = info["module"].replace(".", "/")
    return (
        f"https://forgemia.inra.fr/chip-gt/antipoaching/-/tree/main/{filename}"
    )


# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

# Make sure the automatic target for each section is unique
autosectionlabel_prefix_document = False

# Enable numref
numfig = True

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "sphinx_rtd_theme"
html_static_path = ["_static"]

# -- Options for sphinx-multiversions
# https://github.com/Holzhaus/sphinx-multiversion
# https://blog.stephane-robert.info/post/sphinx-documentation-multi-version/

smv_tag_whitelist = r"^v\d+\.\d+.*$|latest"  # tags of form v*.*.x and latest
# Whitelist pattern for branches (set to '' to ignore all branches)
smv_branch_whitelist = "main"  # Include only main branch
smv_released_pattern = r"^tags/.*$"
smv_remote_whitelist = r"^.*$"
