.. Anti-Poaching Environment (APE) documentation master file, created by

Welcome to Anti-Poaching Environment (APE)'s documentation!
===========================================================

.. figure:: ../../examples/manual_policies/gifs/fixed.gif
   :class: with-border
   :width: 250px
   :align: center

   Instance of APE with 2 Rangers (blue) vs 4 Poachers (red) 

APE is a PettingZoo environment that implements the Anti-Poaching Game. This is a multi-agent, zero-sum, cooperative-competitive game played between a team of Rangers and some independent Poachers on a grid. 

We first quickly introduce :doc:`ape` and how to use it with the given examples. A more detailed explanation of the examples, notably the integration with RLlib is given in :doc:`examples`. :doc:`implementation` lists the entire API for APE and the RLlib integration.

.. toctree::
   ape
   examples
   ape_api
   rllib_api
   CHANGELOG

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
