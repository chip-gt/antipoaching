from setuptools import setup, find_packages

setup(
    name="AntiPoaching-Game",
    version="0.3",
    url="https://forgemia.inra.fr/chip-gt/antipoaching",
    author="Prasnna Maddila",
    author_email="siva-sri-prasanna.maddila@inrae.fr",
    description="Implementation of the AntiPoaching Game as a PettingZoo environment",
    packages=find_packages(),
    install_requires=["ray[rllib] >= 2.8.0", "pettingzoo"],
)
