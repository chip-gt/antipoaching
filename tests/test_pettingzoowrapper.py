"""
Runs PPO on a sample RLlib environment with nested spaces.
"""

import numpy as np
from gymnasium.core import Wrapper
from gymnasium.spaces import Box, Dict, Discrete, MultiDiscrete
from pettingzoo.utils.env import ParallelEnv
from pettingzoo.test import parallel_api_test
import ray
from ray.tune.registry import register_env
from ray.rllib.policy.policy import PolicySpec
from ray.rllib.algorithms.ppo.ppo import PPOConfig
from ray.rllib.env.wrappers.pettingzoo_env import ParallelPettingZooEnv
from ray.tune.logger import pretty_print

import pytest


class DifferentSpacesEnv(ParallelEnv):
    """
    PettingZoo environment that implements different
    observation and action spaces.
    """

    def __init__(self):
        """
        Initialises the class. The logic is not important,
        just that the spaces are different.
        """
        self.agents = ["agent_1", "agent_2"]
        self.possible_agents = self.agents.copy()

        self.observation_spaces = {
            "agent_1": Dict({"a": Discrete(3), "b": Dict({"e": Box(1, 3)})}),
            "agent_2": Dict(
                {"dis": Discrete(30), "dico": MultiDiscrete([11, 11])}
            ),
        }
        self.action_spaces = {"agent_1": Discrete(4), "agent_2": Discrete(5)}
        self.max_time = 1000
        self.curr_time = 0

    def step(self, actions: dict):
        """
        Takes a step and returns the observation,
        rewards, terminated, truncated and info dictionaries,
        each keyed by agent.
        """
        self.curr_time += 1
        rewards = {agent: 1 for agent in self.agents}
        obs = {
            agent: self.observation_spaces[agent].sample()
            for agent in self.agents
        }
        terminated = dict.fromkeys(self.agents, False)
        truncated = dict.fromkeys(self.agents, False)
        info = dict.fromkeys(self.agents, {})

        return obs, rewards, terminated, truncated, info

    def reset(self, seed: int = None, return_info=None, options=False):
        """
        Calls the super reset
        """
        self.agents = self.possible_agents[:]
        for agent in self.agents:
            self.observation_spaces[agent].seed(seed)
            self.action_spaces[agent].seed(seed)

        obs = dict.fromkeys(self.agents, np.zeros(3))
        if not return_info:
            return obs
        return obs, dict.fromkeys(self.agents, False)

    def render(self):
        # render the game
        return "|".join(self.agents)

    def state(self):
        """
        This is a stateless game, therefore, we return None
        """
        return None

    def observation_space(self, agent: str):
        return self.observation_spaces[agent]

    def action_space(self, agent: str):
        return self.action_spaces[agent]


class FlattenerDiffSpacesEnv(Wrapper):
    """
    Custom Observation/Action Flattener for the DifferentSpacesEnv
    environment.
    """

    def __init__(self, env: DifferentSpacesEnv):
        assert isinstance(
            env, DifferentSpacesEnv
        ), "Unsupported environment passed!"
        super().__init__(env)

        # Store the old spaces
        self._obs = self.env.observation_spaces.copy()
        self._acts = self.env.action_spaces.copy()

        # and override them here
        self.env.observation_spaces = dict.fromkeys(
            self.env.agents, Box(0, 30, shape=(3,), dtype=np.int8)
        )
        self.env.action_spaces = dict.fromkeys(self.env.agents, Discrete(5))

    def unwrap(self) -> DifferentSpacesEnv:
        """
        Yields the old enviroment with the old spaces.
        """
        self.env.observation_spaces = self._obs
        self.env.action_spaces = self._acts
        return self.env

    def flatten_obs(self, obs):
        """
        Returns the flattened observations
        """
        return {
            "agent_1": np.array(
                [obs["agent_1"]["a"], *obs["agent_1"]["b"]["e"], 0],
                dtype=np.int8,
            ),
            "agent_2": np.array(
                [obs["agent_2"]["dis"], *obs["agent_2"]["dico"]], dtype=np.int8
            ),
        }

    def unflatten_obs(self, fobs):
        """
        Returns the unflattened observations
        """
        return {
            "agent_1": {
                "a": fobs["agent_1"][0],
                "b": {"e": fobs["agent_1"][1]},
            },
            "agent_2": {
                "dis": fobs["agent_2"][0],
                "dico": fobs["agent_2"][1:],
            },
        }

    def step(self, action):
        """
        Overrides the stop of the DiffSpacesEnv to
        cast the observations into the new type
        """
        # Pre-processing the first agent's actions
        action["agent_1"] = 0 if action["agent_1"] == 5 else action["agent_1"]

        # get the step done
        return self.env.step(action)

    def reset(self, seed: int = None, options=None, return_info=False):
        """Calls the super reset"""
        return self.env.reset(seed, options, return_info)


@pytest.mark.skip(reason="Takes too long")
def test_diff_spaces_env():
    # assert that this is a ParallelEnv
    # This will throw if not.
    env = FlattenerDiffSpacesEnv(DifferentSpacesEnv())
    parallel_api_test(DifferentSpacesEnv())
    parallel_api_test(env)

    # Convert this into a MultiAgentEnv
    ray.init()
    register_env(
        "DifferentSpacesEnv",
        lambda config: ParallelPettingZooEnv(
            FlattenerDiffSpacesEnv(DifferentSpacesEnv(**config))
        ),
    )

    # Run some sample algorithm on it
    config = (
        PPOConfig()
        .environment("DifferentSpacesEnv")
        .training(train_batch_size=1024)
        .framework("torch")
        .multi_agent(
            policies={
                "agent_1": PolicySpec(
                    policy_class=None,  # Infer automatically
                    observation_space=env.observation_space("agent_1"),
                    action_space=env.action_space("agent_1"),
                    config={},
                ),
                "agent_2": PolicySpec(
                    policy_class=None,  # Infer automatically
                    observation_space=env.observation_space("agent_2"),
                    action_space=env.action_space("agent_2"),
                    config={},
                ),
            },  # end-policies
            policy_mapping_fn=lambda ag_id, ep, worker, **kwargs: ag_id,
            policies_to_train=[
                "agent_1",
                "agent_2",
            ],  # train all policies
        )  # end-multi-agent
    )  # end-config

    algo = config.build()
    result = []
    for i in range(10):
        result.append(algo.train())

    algo.stop()
    ray.shutdown()
    for i in range(10):
        print(f"Result at iteration {i}:")
        print(pretty_print(result[i]))
