"""
Tests for the GridStateConstProb helper class. This is important since 
the main state of the ConservationGame environment is
represented by instances of this class.
"""

import pytest
from anti_poaching.anti_poaching_v0 import GridStateConstProb, Trap


def test_gridstate():
    """
    Tests get_surroundings. Uses a grid of size 3 so that
    some actions will definitely be invalid. Otherwise, tests
    that all behaviour is as intended.
    """
    poachers = [f"poacher_{i}" for i in range(5)]
    poacher_traps = {
        poacher: [Trap(f"trap_{i}_{poacher}") for i in range(3)]
        for poacher in poachers
    }
    rangers = [f"ranger_{i}" for i in range(5)]
    g = GridStateConstProb(
        grid_size=3, rangers=rangers, poachers=poachers, ntraps_per_poacher=3
    )
    for agent in poachers + rangers:
        pos = g.state[agent]
        print(agent, g.valid_pos(pos))
        assert g.valid_pos(
            pos
        ), f"Default init must be valid: {agent} @ pos:{pos} (grid_size:{g.N})"

    for poacher in poachers:
        trap = poacher_traps[poacher].pop()
        g.add_trap(trap, g.state[poacher][0:2])

    ctr = 0
    for thing in g.state:
        assert isinstance(thing, Trap) or isinstance(
            thing, str
        ), "Only agents or traps allowed!"
        ctr = ctr + 1 if isinstance(thing, Trap) else ctr

    assert ctr == len(
        poachers
    ), "Each poacher should have placed a trap! ctr:{ctr}"

    for agent in rangers + poachers:
        x, y = g.state[agent][0:2]
        valid_actions = g.permitted_movements(agent)

        # Generating all possible steps,
        # and verifying if they match validity.
        up_pos = (x - 1, y)
        left_pos = (x, y - 1)
        down_pos = (x + 1, y)
        right_pos = (x, y + 1)

        for idx, pos in enumerate([up_pos, left_pos, down_pos, right_pos]):
            assert (
                g.valid_pos(pos) == valid_actions[idx + 1]
            ), "Invalid action allowed!"

    for obj in list(g.state.keys()).copy():
        if isinstance(obj, Trap):
            g.remove_trap(obj)
        else:
            g.remove_poacher(obj)

    for thing in g.state:
        assert isinstance(thing, str), "No traps should have remained!"

    for poacher in poachers:
        assert all(
            g.state[poacher] == GridStateConstProb.NULL_POS
        ), "All poachers should have been retired!"
