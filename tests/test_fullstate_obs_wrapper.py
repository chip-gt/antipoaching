"""
Tests for the GridStateConstProb helper class. This is important since 
the main state of the ConservationGame environment is
represented by instances of this class.
"""

import pytest
from anti_poaching.anti_poaching_v0 import (
    GridStateConstProb,
    FullStateObservationWrapper,
)
from .test_game_env import game_const, game_const
from pettingzoo.test import parallel_api_test


def test_fullstate_obs_wrapper(game_const):
    """Instantiates an environment and tests that each agent
    can see (same) the global state."""

    wrapped_game = FullStateObservationWrapper(env=game_const)
    done, observations = False, None
    action_mask = {
        agent: wrapped_game.grid.permitted_movements(agent)
        for agent in game_const.agents
    }
    while not done:
        actions = {
            agent: wrapped_game.action_space(agent).sample(
                mask=action_mask[agent]
            )
            for agent in wrapped_game.agents
        }
        observations, _, _, _, _ = wrapped_game.step(actions)

        # Verify that each agent receives valid observations
        for agent, space in wrapped_game.observation_space.items():
            assert space.contains(
                observations[agent]
            ), f"{agent} received incompatible observations\n {observations[agent]}"

        # update the possible actions for each agent and update done-status
        action_mask = {
            agent: observations[agent]["action_mask"] for agent in observations
        }
        done = all(terminations.values()) or all(truncations.values())
