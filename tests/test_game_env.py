"""
Personal testing module for the conservation_game
environment and related modules. 
Usage: (in Conservation-Game/)
    $ pytest test/
"""

import sys

sys.path.append("../")

from copy import deepcopy
import pytest
from gymnasium.spaces import Discrete
from pettingzoo.test import (
    parallel_api_test,
    max_cycles_test,
    performance_benchmark,
)
from pettingzoo.test.seed_test import parallel_seed_test
from anti_poaching.anti_poaching_v0 import *


@pytest.fixture()
def game_const():
    """
    Creates a particular instance of the conservation game class
    to be used in all the tests."""
    print("Creating new fixture instance of ConsGame ... ")
    yield anti_poaching.parallel_env(
        grid_size=5,
        npoachers=5,
        nrangers=4,
        ntraps_per_poacher=10,
        prob_detect_cell=0.5,
        prob_animal_appear=0.5,
        prob_detect_trap=0.5,
        max_time=200,
    )
    print("End of fixture ...")


@pytest.fixture()
def game_small():
    print("Creating new fixture instance of ConsGame ... ")
    yield anti_poaching.parallel_env(
        grid_size=3,
        npoachers=5,
        nrangers=4,
        ntraps_per_poacher=10,
        prob_detect_cell=0.5,
        prob_animal_appear=0.5,
        prob_detect_trap=0.5,
        max_time=200,
    )
    print("End of fixture ...")


@pytest.fixture()
def game_vary_pca():
    """
    Creates an instance of the conservation game class
    with varying prob_animal_appear."""
    print("Creating new fixture instance of ConsGame ... ")
    yield anti_poaching.parallel_env(
        grid_size=10,
        npoachers=5,
        nrangers=4,
        ntraps_per_poacher=10,
        prob_detect_cell=0.5,
        prob_animal_appear=[0.2, 0.3],
        prob_detect_trap=0.5,
        max_time=200,
        render_mode="ansi",
        grid_class=GridStateVaryingPCA,
        pca_mode="kernel",
    )
    print("End of fixture ...")


def test_parallel_test_api_suite(game_const, game_vary_pca):
    """
    Runs the parallel_api_test for multiple grid sizes.
    """
    for grid_size in range(1, 3 + 1):
        parallel_api_test(
            anti_poaching.parallel_env(grid_size=grid_size), num_cycles=1000
        )
    parallel_api_test(game_const, num_cycles=1000)
    parallel_api_test(game_vary_pca, num_cycles=1000)


def test_parallel_seed_test():
    """Runs the seed test on the Conservation game."""
    parallel_seed_test(anti_poaching.parallel_env)


def test_environment(game_small, every: int = 20):
    """
    Simulates the environment until done.
    """
    done = False
    observations, terminations, truncations = (
        None,
        None,
        None,
    )
    action_mask = {
        agent: game_small.grid.permitted_movements(agent)
        for agent in game_small.agents
    }
    print("-" * 80)
    while not done:
        # sample the actions for each agent
        actions = {
            agent: game_small.action_space(agent).sample(
                mask=action_mask[agent]
            )
            for agent in game_small.agents
        }
        # step through the environment
        _step_objs = game_small.step(actions)
        observations, rewards, terminations, truncations, infos = _step_objs

        # Verify that all agents are alive !!! Recall that poachers are alive,
        # but in a caught state.
        time_stat = game_small.curr_time < game_small.max_time
        for obj in _step_objs:
            assert set(obj.keys()) == set(
                game_small.possible_agents
            ), f"All agents should recieve things until end-of-game (current time = {game_small.curr_time})"

        # Verify that each agent receives valid observations and actions !!!
        for agent, space in game_small.observation_spaces.items():
            assert space.contains(
                observations[agent]
            ), f"{agent} received incompatible observations\n {observations[agent]}"

        for agent, space in game_small.action_spaces.items():
            assert space.contains(
                actions[agent]
            ), f"{agent} received incompatible action\n {actions[agent]}"

        # update the possible actions for each agent
        action_mask = {
            agent: observations[agent]["action_mask"] for agent in observations
        }

        # post-processing
        done = all(terminations.values()) or all(truncations.values())
        if game_small.curr_time % every == 0 or done:
            game_small.render()  # This should not bug out
            print("-" * 80)

    print(game_small.curr_time, " is the current time")
    print(game_small.max_time, " is the maximum time")
    print("Rewards: ")
    assert sum(rewards.values()) == 0.0, "Game is not zero-sum !!!"


def test_init_game_small(game_const):
    """
    Tests the initialisation of the ConsGame
    """
    # Check the lists of created agents
    assert len(game_const.poachers) == 5, "Too many or few poachers created."
    assert len(game_const.rangers) == 4, "Too many or few rangers created."
    assert set(game_const.agents) == set(
        game_const.rangers + game_const.poachers
    ), "Number of agents does not match the set of agents"
    assert (
        sum((len(traps) for _, traps in game_const.poacher_traps.items()))
        == 10 * 5
    ), "Number of traps is wrong!"
    assert all(
        (
            Discrete(5) == game_const.action_spaces[ranger]
            for ranger in game_const.rangers
        )
    ), "Incorrect Ranger action space created."

    # Check the observation and action spaces.
    assert set(game_const.rangers + game_const.poachers) == set(
        game_const.action_spaces.keys()
    ), "Not all agents have actions, or there are too many agents."
    assert all(
        (
            Discrete(6) == game_const.action_spaces[poacher]
            for poacher in game_const.poachers
        )
    ), "Incorrect Poacher action space created."

    assert set(game_const.rangers + game_const.poachers) == set(
        game_const.observation_spaces.keys()
    ), "Not all agents have observations, or there are too many agents."

    obs_keys = {"observations", "action_mask"}
    for agent in game_const.agents:
        _space = game_const.observation_space(agent)
        _sample = _space.sample()
        assert set(_space.keys()) == obs_keys
        if "ranger" in agent:
            assert (
                len(_sample["observations"]) == game_const._ranger_obs_size
            ), "Ranger obs space of incorrect size."
        else:
            assert (
                len(_sample["observations"]) == game_const._poacher_obs_size
            ), "Poacher obs space of incorrect size."


def test_reset(game_const):
    """
    Tests the reset function of the AntiPoachingGame environment.
    """
    lcopy = deepcopy(game_const)

    # Simulating a game.
    game_const.agents.remove("poacher_1")
    game_const.poacher_traps["poacher_1"] = []

    assert set(lcopy.agents) != set(
        game_const.agents
    ), "Removed agent does not change agents set."
    assert set(lcopy.poacher_traps["poacher_1"]) != set(
        game_const.poacher_traps["poacher_1"]
    ), "Removing traps does not change traps set."
    assert all(
        (
            set(lcopy.poacher_traps[poacher])
            == set(game_const.poacher_traps[poacher])
            for poacher in game_const.poachers
            if "poacher_1" != poacher
        )
    ), "Other agents should be unaffected by the change."

    # reset should change this behavious
    game_const.reset()
    assert set(lcopy.agents) == set(
        game_const.agents
    ), "Reset does not reset agents list."
    assert all(
        (
            set(lcopy.poacher_traps[poacher])
            == set(game_const.poacher_traps[poacher])
            for poacher in lcopy.poachers
        )
    ), "Reset does not reset poachers' traps."


def test_assign_reward(game_small):
    """
    Tests all the branches of the _assign_reward helper
    function
    """
    for poacher in game_small.poachers.copy():
        rewards = dict.fromkeys(game_small.agents, 0)

        # First if we assign a reward to a poacher
        game_small._assign_reward(poacher, 1, rewards)
        assert rewards[poacher] == 1
        assert sum((rewards[ranger] for ranger in game_small.rangers)) == -1

        # Force assigning a reward after the poacher is `caught`
        game_small.grid.state[poacher] = game_small.grid.NULL_POS
        game_small._assign_reward(poacher, -1, rewards)
        assert rewards[poacher] == 0
        assert (
            sum((rewards[ranger] for ranger in game_small.rangers)) == 0
        ), "ranger rewards not updated when assigning reward to retired agent"


def test_observation_time(game_small):
    """Tests if the remaining time observation
    is well implemented"""
    obs, infos = game_small.reset()
    for agent in game_small.agents:
        assert (
            game_small.max_time == obs[agent]["observations"][0]
        ), f"{agent} is missing remaining_time attr in obs"

    for t in range(1, game_small.max_time + 1):
        action_mask = {
            agent: game_small.grid.permitted_movements(agent)
            for agent in game_small.agents
        }
        actions = {
            agent: game_small.action_space(agent).sample(
                mask=action_mask[agent]
            )
            for agent in game_small.agents
        }
        obs, _, _, _, _ = game_small.step(actions)

        # Assert that we got the right time
        assert (
            game_small.curr_time == t
        ), f"{game_small.curr_time=} is not matching stepped timesteps {t}"

        for agent in game_small.agents:
            # and that each agent receives this as well.
            assert (game_small.max_time - t) == obs[agent]["observations"][
                0
            ], f"{agent} has bad updates for remaining_time attr in obs"
