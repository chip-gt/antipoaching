import ray
from ray import tune
from gymnasium import spaces
from ray.rllib.examples.env.random_env import RandomEnv
from ray.rllib.utils.framework import try_import_torch
import torch
import torch.nn.functional as F

from examples.rllib.recurrent import LSTMActionMaskingModel


def test_random_action_masked_env():
    """Test to check if the LSTMActionMaskingModel works
    on a Random Action Masked Environment."""
    config = {
        "env": RandomEnv,
        "env_config": {
            "observation_space": spaces.Dict(
                {
                    "observations": spaces.Discrete(5),
                    "action_mask": spaces.MultiBinary(5),
                }
            ),
            "action_space": spaces.Discrete(5),
            "max_episode_len": 10,
        },
        "model": {
            "custom_model": LSTMActionMaskingModel,
            "custom_model_config": {"no_masking": False},
            "use_lstm": False,
            "lstm_cell_size": 512,
        },
        "num_gpus": 0,
        "num_workers": 1,
        "framework": "torch",
        "_enable_rl_module_api": False,
        "_enable_learner_api": False,
        "disable_env_checking": False,
    }

    stop = {
        "training_iteration": 1,
    }

    ray.init(num_cpus=4)
    results = tune.run("PPO", config=config, stop=stop, verbose=2)
    ray.shutdown()
