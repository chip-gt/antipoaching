"""
Module to test the RLlib installation. This is meant to be run locally.
"""

import ray
from ray.rllib.policy.policy import PolicySpec
from ray.rllib.examples.env.multi_agent import MultiAgentCartPole
from ray.rllib.algorithms.ppo.ppo import PPOConfig

import pytest


@pytest.mark.skip(reason="Takes too long")
def test_installation():
    """
    Function to test the installation of PyTorch and RLlib
    """
    ray.init(local_mode=True)
    config = (
        PPOConfig()
        .environment(MultiAgentCartPole, env_config={"num_agents": 2})
        .framework("torch")
        .multi_agent(
            policies={
                "default_policy": PolicySpec(
                    policy_class=None,
                    observation_space=None,
                    action_space=None,
                    config={},
                ),
            },  # end-policies
        )  # end-multi-agent
    )  # end-config

    # Build the algo, and define run-criteria
    algo = config.build()

    # if the algo runs for an iteration, success.
    algo.train()

    algo.stop()
    ray.shutdown()
