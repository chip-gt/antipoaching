"""
Module to show the example use case of the AntiPoachingGame 
environment. This program simulates a random strategy for 
each player.
"""

from pprint import pprint
from anti_poaching.anti_poaching_v0 import grouped_rllib_cons_game

if __name__ == "__main__":
    cg = grouped_rllib_cons_game()
    _base_cg_env = cg.env.unwrapped

    for iteration in range(3):
        done, terminations, truncations = False, None, None
        observations, infos = cg.reset()

        pprint(observations)
        pprint(f"GAME BEGIN {iteration}" + "-" * 40)
        while not done:
            # sample the actions for each agent randomly
            action_mask = [
                tuple(
                    observations[group][idx]["action_mask"]
                    for idx, agent in enumerate(getattr(_base_cg_env, group))
                )
                for group in cg.groups
            ]
            actions = {
                group: cg.action_space[gid].sample(mask=action_mask[gid])
                for gid, group in enumerate(cg.groups)
            }
            # step through the environment
            observations, rewards, terminations, truncations, _ = cg.step(
                actions
            )
            pprint(observations)

            # post-processing: find out if the game is over
            # and give relevant outputs.
            done = all(
                x or y
                for x, y in zip(terminations.values(), truncations.values())
            )
        pprint(f"GAME END {iteration}" + "-" * 40)
