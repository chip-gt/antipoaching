"""
Module to show the example use case of the AntiPoachingGame 
environment. This program simulates a strategy written 
à la main for each player. 
"""

import sys
import pathlib
import numpy as np
import pygame
from anti_poaching.anti_poaching_v0 import anti_poaching


def random_policy(action_mask: np.array):
    """
    Takes an action mask and randomly chooses
    an available action.
    """
    return np.random.choice(
        np.arange(action_mask.shape[0]), p=action_mask / np.sum(action_mask)
    )


def static_poacher_policy(action_mask: np.array):
    """
    Greedy policy for poachers where they place
    a trap if they can, otherwise they randomly
    choose a movement action.
    """
    if action_mask[5]:
        print("Choosing to place trap ...")
        return 5
    return np.random.choice(
        np.arange(action_mask[0:5].shape[0]),
        p=action_mask[0:5] / np.sum(action_mask[0:5]),
    )


if __name__ == "__main__":
    is_save = (
        True if len(sys.argv) > 1 and sys.argv[1].startswith("s") else False
    )
    cg = anti_poaching.parallel_env(
        nrangers=2, npoachers=4, grid_size=10, render_mode="rgb", seed=123
    )

    if is_save:
        output_path = pathlib.Path(pathlib.Path.cwd() / "img_fixed")
        output_path.mkdir(exist_ok=True)
        screen = cg.grid.screen
        itr = 0

    done = False
    observations, terminations, truncations = (
        None,
        None,
        None,
    )
    action_mask = {
        agent: cg.grid.permitted_movements(agent) for agent in cg.agents
    }
    print("GAME BEGIN" + "-" * 40)
    while not done:
        # Each poacher gets a greedy strategy where they place as
        # many traps as they can. Rangers get a random policy where
        # they just randomly patrol the grid.
        actions = {
            **{
                poacher: static_poacher_policy(action_mask[poacher])
                for poacher in cg.poachers
                if poacher in cg.agents
            },
            **{
                ranger: random_policy(action_mask[ranger])
                for ranger in cg.rangers
            },
        }

        # few runtime-checks
        for agent in cg.agents:
            assert cg.action_space(agent).contains(
                actions[agent]
            ), f"Incompatible action space detected for {agent}-> action:{actions[agent]}"

        print("PRE-STEP:")
        for agent in cg.agents:
            print(f"\t{agent} is in old state:{cg.grid.state[agent]}")
            print(f"\t{agent} has chosen action:{actions[agent]}\n")

        # step through the environment
        print("STEP: ")
        observations, _, terminations, truncations, _ = cg.step(actions)
        print()

        # update the possible actions for each agent
        action_mask = {
            agent: observations[agent]["action_mask"] for agent in cg.agents
        }

        # post-processing
        done = all(
            x or y for x, y in zip(terminations.values(), truncations.values())
        )

        # Examine the new state
        print("POST-STEP:")
        for agent in cg.agents:
            print(f"\t{agent} is in new state:{cg.grid.state[agent]}")
        print()

        cg.render()
        if is_save:
            pygame.image.save(screen, output_path / f"screen_{itr}.png")
            itr += 1
        print("-" * 40)

    print("\n GAME OVER !\n")
    print(cg.curr_time, " is the current time")
    print(cg.max_time, " is the maximum time")
