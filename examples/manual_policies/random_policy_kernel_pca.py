"""
Module to show the example use case of the AntiPoachingGame 
environment using the GridStateVaryingPCA (mode: kernel) state,
and with random policies for the agents. This means that the Grid has 
different probabilities of animal appearance per cell, and specifically
in the kernel mode, this peaks at the center and dies out at the edge.

Note: Since the poachers are following a heuristic that does not exploit 
this information, or learn from the surroundings, this should not change 
anything for the agents. This file is used mainly to test.
"""

import sys
import pathlib
import pygame
from anti_poaching.anti_poaching_v0 import anti_poaching, GridStateVaryingPCA

if __name__ == "__main__":
    render_mode = sys.argv[1] if len(sys.argv) > 1 else "rgb"
    is_save: bool = (
        True if len(sys.argv) > 2 and sys.argv[2] == "save" else False
    )
    cg = anti_poaching.parallel_env(
        grid_size=10,
        render_mode=render_mode,
        grid_class=GridStateVaryingPCA,
        prob_animal_appear=[0.2, 0.3],
        pca_mode="random",
        seed=123,
    )
    done = False
    observations, terminations, truncations = (
        None,
        None,
        None,
    )
    action_mask = {
        agent: cg.grid.permitted_movements(agent) for agent in cg.agents
    }

    if is_save:
        output_path = pathlib.Path(pathlib.Path.cwd() / "img_varying_pca")
        output_path.mkdir(exist_ok=True)
        screen = cg.grid.screen
        itr = 0

    print("GAME BEGIN" + "-" * 40)
    while not done:
        # sample the actions for each agent randomly
        print(action_mask)
        actions = {
            agent: cg.action_space(agent).sample(mask=action_mask[agent])
            for agent in cg.agents
        }
        # step through the environment
        print("PRE-STEP:")
        for agent in cg.agents:
            print(f"\t{agent} is in old state:{cg.grid.state[agent]}")
            print(f"\t{agent} has chosen action:{actions[agent]}\n")

        print("STEP: ")
        observations, _, terminations, truncations, _ = cg.step(actions)
        print()

        # update the possible actions for each agent
        action_mask = {
            agent: observations[agent]["action_mask"] for agent in cg.agents
        }

        # post-processing: find out if the game is over
        # and give relevant outputs.
        done = all(
            x or y for x, y in zip(terminations.values(), truncations.values())
        )
        print("POST-STEP:")
        for agent in cg.agents:
            print(f"\t{agent} is in new state:{cg.grid.state[agent]}")
        print()

        cg.render()
        if is_save:
            pygame.image.save(screen, output_path / f"screen_{itr}.png")
            itr += 1
        print("-" * 40)

    print("\n GAME OVER !\n")
    print(cg.curr_time, " is the current time")
    print(cg.max_time, " is the maximum time")
