# Examples

This folder contains examples to use the Anti-Poaching Game as developed in [anti_poaching_game.py](../anti_poaching/env/anti_poaching_game.py). 

- [manual_policies/](manual_policies/) contains examples with explicit learning loops and custom policies like `Static` and `Random`. These illustrate how the learning loop is executed outside of any frameworks like RLlib. 

- [rllib/](rllib/) contains more examples implemented using the RLlib framework. This implements Independent Learner policies using PPO and PG, as well as cooperative algorithms like QMix. It also implements the approximate exploitability measure ($AEXPL$) as a callback in [callbacks:ExploitabilityCallback](rllib/callbacks.py). Lastly, it also implements a visualisation tool, [from_checkpoint.py](rllib/from_checkpoint.py), which can visualise a saved checkpoint created by the main script.


## Manual Examples

The manual policy examples currently implement 

- [fixed_policy.py](manual_policies/fixed_policy.py) implements the Static policy for poachers and a random policy for rangers. Note that we reimplement these policies in RLlib as well. It additionally gives the option to record the realised episode to a sequence of frames, as
```bash
$ python manual_policies/fixed_policy.py save
```
- [random_policy.py](manual_policies/random_policy.py) implements the pure random strategy, where all agents take a random legal move at each step.
- In [random_policy_kernel_pca.py](manual_policies/random_policy_kernel_pca.py), the $p_{CA}$ or `--prob-anim` parameter is varied according to either _random_ or _kernel_ modes. The rangers follow the same pure random strategy as before. This example is to illustrate the possible variations in the environment.

## RLlib Examples

### main.py

The main script to run is `main.py`. It can be configured by using a variety of command line options, which can change the parameters of the Anti-Poaching game, the algorithm used to train on it or its parameters, or other things like stopping criteria. All these arguments have default values, so you can simply run

```bash
$ python rllib/main.py
```

Otherwise, to get a full list of modifiable parameters, use

```bash
$ python rllib/main.py --help

usage: main.py [-h] [--algo ALGO] [--use-hyper] [--no-expl] [--verbose]
               [--num-cpus NUM_CPUS] [--num-gpus NUM_GPUS]
               [--num-cpus-per-worker NUM_CPUS_PER_WORKER]
               [--eval-every EVAL_EVERY] [--eval-workers EVAL_WORKERS]
               [--timesteps TIMESTEPS] [--rangers RANGERS]
               [--poachers POACHERS] [--grid GRID] [--max_time MAX_TIME]
               [--ntraps NTRAPS] [--prob-cell PROB_CELL]
               [--prob-trap PROB_TRAP] [--prob-anim PROB_ANIM]
               [--trial-name TRIAL_NAME] [--policies-train POLICIES_TRAIN]
               [--rpol RPOL] [--ppol PPOL]
... 
```

### Visualising Saved Policies

When `main.py` runs, it saves a checkpoint at every evaluation iteration, and once at the end, if necessary to the hidden directory `.results/`. To visualise these saved policies, the supplied [from_checkpoint.py](rllib/from_checkpoint.py) script is used. Given a checkpoint directory `rllib/.results/PPO/PPOTrial/checkpoint_000001`, we can visualise an episode as 
```bash 
$ python rllib/from_checkpoint.py rllib/.results/PPO/PPOTrial/checkpoint_000001
```

There are a few caveats. Firstly, the supplied path must point to the algorithm checkpoint directory and not to the policy checkpoints. Secondly, `main.py` also saves the environment configuration to `env_config.json` in the `PPOTrial` directory, which is used to recreate the environment used during the original policy. Without this JSON file, the program will crash.


### Hyper-parameter Tuning

The supplied [tune_antipoaching.py](rllib/tune_antipoaching.py) script will launch the hyperparameter tuning for a supported configuration (PPO/PG/QMix). All the parameters of `main.py` are accessible, so configuring an algorithm for a particular training scenario (Cooperative / Cooperative-Competitive), or for a particular environment is possible. To run the a configuration similar to the paper, we can run,

```bash
$ python rllib/tune_antipoaching.py --num-samples 100 --stop-timesteps 250000 
                --eval-every 250000 --algo PG --competititve --expl --num-cpus 20
```

A number of execution scripts can also be found in [DEPRECATED](rllib/DEPRECATED/), which were used to launch experiments on remote servers.
