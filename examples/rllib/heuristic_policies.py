"""
This module defines the heuristic strategies that can be used as policies
for non-learning agents. This currently uses :class:`ActionMaskedRandomPolicy`
which implements the pure random strategy, :class:`ActionMaskedStaticPoacherPolicy`
which implements the Static strategy and the deprecated :class:`ActionMaskedGreedyPoacherPolicy`.

This also reimplements the same policies for QMix, required due to incompatibility reasons.
This refers to the :class:`QMIXActionMaskedRandomPolicy` and :class:`QMIXGreedyStaticPolicy`
respectively. All QMix policies inherit from the :class:`QMIXBaseHeuristicPolicy` to 
eliminate a lot of boilerplate. By doing this, future heuristic policies only need to 
implement :meth:`QMIXBaseHeuristicPolicy._compute_actions_from_obs` method.
"""

from typing import Generator
from anti_poaching.env.utils.typing import *
from collections import OrderedDict
import pathlib
import numpy as np
import torch
from abc import abstractmethod

# Ray imports
from ray.rllib.policy import Policy
from ray.rllib.policy.policy import PolicySpec
from ray.rllib.utils.annotations import override
from ray.rllib.policy.torch_policy import TorchPolicy
from ray.rllib.policy.sample_batch import SampleBatch
from ray.rllib.algorithms.qmix.qmix_policy import QMixTorchPolicy, _mac
from ray.rllib.models.torch.torch_action_dist import TorchCategorical


class ActionMaskedRandomPolicy(Policy):
    """Random policy with action masking"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.exploration = self._create_exploration()

    def get_initial_state(self):
        return [0]

    def is_recurrent(self) -> bool:
        return True

    def compute_actions(
        self,
        obs_batch,
        state_batches=None,
        prev_action_batch=None,
        prev_reward_batch=None,
        info_batch=None,
        episodes=None,
        **kwargs,
    ):
        if isinstance(obs_batch, (OrderedDict, dict)):
            action_mask = np.array(
                obs_batch["action_mask"], dtype=BOOL
            ).flatten()
        else:
            action_mask = obs_batch[0][: self.action_space.n].astype(BOOL)
        return (
            [self.action_space.sample(action_mask)],
            state_batches,
            {},
        )

    def learn_on_batch(self, samples):
        pass

    def get_weights(self):
        pass

    def set_weights(self, weights):
        pass


class ActionMaskedGreedyPoacherPolicy(Policy):
    """Greedy policy with action masking: Will place traps if possible,
    else choose a movement action."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.exploration = self._create_exploration()

    def get_initial_state(self):
        return [0]

    def is_recurrent(self) -> bool:
        return True

    def compute_actions(
        self,
        obs_batch,
        state_batches=None,
        prev_action_batch=None,
        prev_reward_batch=None,
        info_batch=None,
        episodes=None,
        **kwargs,
    ):
        if isinstance(obs_batch, (OrderedDict, dict)):
            action_mask = np.array(
                obs_batch["action_mask"], dtype=BOOL
            ).flatten()
        else:
            action_mask = obs_batch[0][: self.action_space.n].astype(BOOL)
        if action_mask[5]:
            action = 5
        else:
            action = np.random.choice(
                np.arange(action_mask.shape[0]),
                p=action_mask / np.sum(action_mask),
            )
        return (
            [action],
            state_batches,
            {},
        )

    def learn_on_batch(self, samples):
        pass

    def get_weights(self):
        pass

    def set_weights(self, weights):
        pass


def _gen_path(
    curr_pos: np.array, fin_pos: np.array, grid_size: int
) -> Generator[int, None, None]:
    """Generates the next path to follow. Last move is always
    place trap, since that's the action to take in a target."""
    MOVES = ((-1, 0), (0, -1), (1, 0), (0, 1))
    moves = []
    pos = curr_pos
    counter = 0
    while _dist(pos, fin_pos) > 0:
        for move, effect in enumerate(MOVES):
            new_pos = pos + effect
            if _valid_pos(new_pos, grid_size) and (
                _dist(new_pos, fin_pos) < _dist(pos, fin_pos)
            ):
                # Only choose a move that is valid and moves closer.
                # Since we move on Manhattan distance, first match is optimal.
                moves.append(1 + move)  # add offset since move 0 is NULL
                pos = new_pos
                break
            else:
                counter += 1
    moves.append(5)
    return iter(moves)


def _dist(left: np.array, right: np.array) -> int:
    """Calculate the Manhattan distance between two points in the grid"""
    return np.sum(np.abs(left - right))


def _valid_pos(pos: np.array, grid_size: int) -> bool:
    """Checks if a position is valid or not"""
    return all(0 <= coord < grid_size for coord in pos)


class ActionMaskedStaticPoacherPolicy(Policy):
    """Static policy with action masking: Will place traps at pre-chosen targets,
    and will periodically cycle between these locations to retrieve or place traps.
    """

    def __init__(self, *args, **kwargs):
        assert (
            "poacher" in args[2]["__policy_id"]
        ), "Cannot use this policy for non-poacher agents!"
        super().__init__(*args, **kwargs)
        self.exploration = self._create_exploration()

        # Get the required variables from the env_config used.
        env_config = args[2]["env_config"]
        self.grid_size = env_config["grid_size"]
        self.max_time = env_config["max_time"]
        self.ntraps_per_poacher = env_config["ntraps_per_poacher"]

        # Store the moves and their effects.
        # UP, LEFT, DOWN, RIGHT
        self.moves = iter([])  # No plan at beginning

    def get_initial_state(self):
        return [0]

    def is_recurrent(self) -> bool:
        return True

    def compute_actions(
        self,
        obs_batch,
        state_batches=None,
        prev_action_batch=None,
        prev_reward_batch=None,
        info_batch=None,
        episodes=None,
        **kwargs,
    ):
        if isinstance(obs_batch, (OrderedDict, dict)):
            flat_obs = obs_batch["observations"].flatten()
            flat_mask = obs_batch["action_mask"].flatten()
        else:
            flat_obs = obs_batch.flatten()
            flat_mask, flat_obs = flat_obs[:6], flat_obs[6:]

        rem_time, curr_pos = flat_obs[0], flat_obs[1:3]

        if all(curr_pos == [-1, -1]):
            # Poacher is captured, only one legal action to take
            # Need to initialise explicitly since _gen_path
            # does not handle for captured poachers.
            action = 0
        else:  # Execution as usual.
            if rem_time == self.max_time:
                # Generate the poaching targets
                # at the beginning of each episode.
                self.targets = [
                    np.random.randint(0, self.grid_size, 2)
                    for _ in range(self.ntraps_per_poacher)
                ]
                self.curr_target = 0

            try:
                action = next(self.moves)
            except StopIteration:
                # Move buffer is empty.
                # Select next target, and regenerate path
                self.curr_target = (self.curr_target + 1) % len(self.targets)
                self.moves = _gen_path(
                    curr_pos, self.targets[self.curr_target], self.grid_size
                )
                action = next(self.moves)

            # Apply the action mask - if placed trap captured, then
            # place trap cannot be valid.
            action = action if flat_mask[action] else 0
        return (
            [action],
            state_batches,
            {},
        )

    def compute_single_action(self, *args, **kwargs):
        obs_batch = kwargs.pop("obs")
        action, sbatches, rest = self.compute_actions(
            obs_batch, *args, **kwargs
        )
        return action[0], sbatches, rest

    def learn_on_batch(self, samples):
        pass

    def get_weights(self):
        pass

    def set_weights(self, weights):
        pass


class QMIXBaseHeuristicPolicy(QMixTorchPolicy):
    """Basic QMIX Policy, meant to be overidden.
    This includes compatibility logic for QMIX."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.exploration = self._create_exploration()
        self.__state = self.get_state()

    @override(QMixTorchPolicy)
    def compute_actions_from_input_dict(
        self,
        input_dict: dict,
        explore: bool = None,
        timestep: int = None,
        **kwargs,
    ) -> tuple:
        obs_batch = input_dict[SampleBatch.OBS]
        state_batches = []
        i = 0
        while f"state_in_{i}" in input_dict:
            state_batches.append(input_dict[f"state_in_{i}"])
            i += 1

        explore = explore if explore is not None else self.config["explore"]
        obs_batch, action_mask, _ = self._unpack_observation(obs_batch)

        # Compute actions
        with torch.no_grad():
            q_values, hiddens = _mac(
                self.model,
                torch.as_tensor(
                    obs_batch, dtype=torch.float, device=self.device
                ),
                [
                    torch.as_tensor(
                        np.array(s), dtype=torch.float, device=self.device
                    )
                    for s in state_batches
                ],
            )

            # Compute actions for each group
            # This is the custom logic that each Heuristic QMIX agent
            # needs to implement.
            actions = self._compute_actions_from_obs(obs_batch, action_mask)

            # no idea what hiddens does, but required for now.
            hiddens = [s.cpu().numpy() for s in hiddens]

        return (
            tuple(actions.transpose([1, 0])),
            hiddens,
            {},
        )

    @abstractmethod
    def _compute_actions_from_obs(self, obs_batch, action_mask) -> np.array:
        """Implement the custom logic to choose actions"""
        raise NotImplementedError

    @override(QMixTorchPolicy)
    def learn_on_batch(self, samples):
        pass

    @override(QMixTorchPolicy)
    def compute_actions(self, *args, **kwargs):
        return self.compute_actions_from_input_dict(*args, **kwargs)


class QMIXActionMaskedRandomPolicy(QMIXBaseHeuristicPolicy):
    """Random policy with action masking,
    using default compatibility logic for QMIX."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @override(QMIXBaseHeuristicPolicy)
    def compute_actions_from_input_dict(self, *args, **kwargs) -> tuple:
        return super().compute_actions_from_input_dict(*args, **kwargs)

    @override(QMIXBaseHeuristicPolicy)
    def _compute_actions_from_obs(self, obs_batch, action_mask) -> tuple:
        """Implement the random action choice
        based on available legal actions."""

        # Generate action distribution
        new_action_mask = action_mask.copy()
        new_action_mask = new_action_mask.reshape(
            (self.n_agents, self.n_actions)
        )
        np.nan_to_num(new_action_mask, copy=False)

        # normalising ...
        new_action_mask /= new_action_mask.sum(axis=1)[:, None]

        # sampling actions and reshaping as required.
        return (
            np.array(
                [
                    np.random.choice(
                        list(range(self.n_actions)),
                        p=new_action_mask[i, :],
                    )
                    for i in range(self.n_agents)
                ]
            )
            .reshape((1, self.n_agents))
            .astype(INTEGER)
        )


class QMIXGreedyStaticPolicy(QMIXBaseHeuristicPolicy):
    """Static Greedy Policy for QMIX-based Poacher agents.
    Each Poacher Policy decides on a few targets at the
    beginning of each episode, and periodically visits each one."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        env_config = args[2]["env_config"]
        self.grid_size = env_config["grid_size"]
        self.max_time = env_config["max_time"]
        self.ntraps_per_poacher = env_config["ntraps_per_poacher"]

        # Store the moves and their effects.
        # UP, LEFT, DOWN, RIGHT
        self.moves = [iter([]) for _ in range(self.n_agents)]

        # To define planning objectives
        self.targets = [[] for _ in range(self.n_agents)]
        self.curr_target = [0 for _ in range(self.n_agents)]

    @override(QMIXBaseHeuristicPolicy)
    def compute_actions_from_input_dict(self, *args, **kwargs) -> tuple:
        return super().compute_actions_from_input_dict(*args, **kwargs)

    @override(QMIXBaseHeuristicPolicy)
    def _compute_actions_from_obs(self, obs_batch, action_mask) -> tuple:
        """Implement the random action choice based on available legal actions."""

        flat_obs = (
            obs_batch.squeeze().astype(INTEGER).reshape((self.n_agents, -1))
        )
        flat_mask = (
            action_mask.squeeze().astype(BOOL).reshape((self.n_agents, -1))
        )
        actions = np.zeros(self.n_agents)

        for a_id in range(self.n_agents):
            # Get current agent's observations and mask
            obs, mask = flat_obs[a_id, :], flat_mask[a_id, :]
            rem_time, curr_pos = obs[0], obs[1:3]

            if all(curr_pos == [-1, -1]):
                # Poacher is captured, only one legal action to take
                action = 0
                continue

            # Generate the poaching targets
            # at the beginning of each episode.
            if rem_time == self.max_time:
                self.targets[a_id] = [
                    np.random.randint(0, self.grid_size, 2)
                    for _ in range(self.ntraps_per_poacher)
                ]
                self.curr_target[a_id] = 0

            # Get action if possible,
            # else regenerate move buffer and try again.
            try:
                action = next(self.moves[a_id])
            except StopIteration:
                # Move buffer is empty.
                # Select next target, and regenerate path
                self.curr_target[a_id] = (self.curr_target[a_id] + 1) % len(
                    self.targets[a_id]
                )
                self.moves[a_id] = _gen_path(
                    curr_pos,
                    self.targets[a_id][self.curr_target[a_id]],
                    self.grid_size,
                )
                action = next(self.moves[a_id])

            # Apply the action mask - if placed trap captured, then
            # place trap cannot be valid.
            action = action if mask[action] else 0
            actions[a_id] = action

        actions = actions.reshape((1, self.n_agents)).astype(INTEGER)
        return actions
