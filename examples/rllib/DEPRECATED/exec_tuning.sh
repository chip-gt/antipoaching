#!/bin/bash
# Defining common flags
declare -i STOP_TIMESTEPS=250000
declare -i EVAL_EVERY=245000
declare -i NUM_SAMPLES=100

PYTHON=python3.8
PROG=tune_antipoaching.py
COMMON_FLAGS=(
 "--is-competitive"
 "--use-memory"
 "--stop-timesteps ${STOP_TIMESTEPS}"
 "--eval-every ${EVAL_EVERY}"
 "--num-samples ${NUM_SAMPLES}"
 "--num-cpus $(nproc --all)"
 "--num-gpus 0.1"
 "--verbose"
)

for ALGO in "$@"
do	
    TUNE_DISABLE_STRICT_METRIC_CHECKING=1 ${PYTHON} ${PROG} --algo ${ALGO} ${COMMON_FLAGS[@]} 
done
