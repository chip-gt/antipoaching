"""
Module that implements PPO for the ConservationGame and 
measures custom metrics. Based on the example at 
https://github.com/ray-project/ray/blob/master/rllib/examples/hist_data_and_callbacks.py#L81C1-L87C14

The custom callbacks also include callbacks for before and after calls to Algorithm.evaluate();
This is used to create checkpoints of all policies that are about to be evaluated.

To train on a 2Rx2P instance on a 3x3 grid, the following is suggested.
$ python3 pg_custom_metrics.py --iters 10 --num-cpus 6 --num-cpus-per-worker 1 \
                             --rangers 2 --poachers 2 --grid 5 \
                             --eval-workers 1 --eval-every 5

NOTE: Evaluation with callbacks will use double the CPU requirements of an experiment ! This is because it launches Tune instances internally which will not reuse the 
      CPUs used for training.
"""

# Core modules
import logging
from datetime import datetime
from typing import Dict, Tuple

# Core libraries
import numpy as np
import gymnasium as gym

# Ray imports
import ray
from ray import air, tune
from ray.tune.logger import pretty_print
from ray.rllib.algorithms.algorithm import Algorithm
from ray.rllib.algorithms.pg import PGConfig

# For typing information
from ray.rllib.policy.policy import PolicySpec
from ray.rllib.env import BaseEnv
from ray.rllib.evaluation import Episode, RolloutWorker
from ray.rllib.algorithms.callbacks import DefaultCallbacks

# Importing custom code
from conservation_game.conservation_game_v0 import (
    ConservationGame,
    rllib_cons_game,
    parse_obs,
)
from example_utils import get_args, define_game_from_args


def pg_config(
    env: ConservationGame,
    env_config: dict = None,
    policies_to_train=None,
    num_gpus: int = 0,
) -> PGConfig:
    """Builds the default PPO Config for ConservationGame"""
    if env_config is None:
        env_config = {}
    if policies_to_train is None:
        policies_to_train = env.agents  # train everyone by default
    return (
        PGConfig()
        .environment(
            ConservationGame.metadata["name"], env_config=env_config
        )  # Pass config to training env !!!
        .framework("torch")
        .resources(num_gpus=num_gpus)
        .training(
            model={
                "custom_model": "TorchActionMaskModel",
                "custom_model_config": {
                    "no_masking": False
                },  # Required by the example
            },
            # Hyperparameters obtained after tuning
            gamma=0.9,
            lr=0.004,
            train_batch_size=128,
        )
        .multi_agent(
            policies={
                agent: PolicySpec(
                    None,  # infer automatically
                    observation_space=env.observation_space[agent],
                    action_space=env.action_space[agent],
                )
                for agent in env.agents
            },
            policy_mapping_fn=lambda aid, episode, worker, **kw: aid,
            policies_to_train=policies_to_train,
        )  # end-multi-agent
    )  # end-config


class RestoreWeightsCallback(DefaultCallbacks):
    current_weights: Dict = None

    @classmethod
    def set_current_weights(cls, current_weights: Dict) -> None:
        cls.current_weights = current_weights

    def on_algorithm_init(self, *, algorithm: "Algorithm", **kwargs) -> None:
        """Reset the weights on algorithm init"""
        assert (
            self.current_weights is not None
        ), "Trying to set empty weights to {algorithm=}"
        algorithm.set_weights(self.current_weights)


class ConservationCallbacks(DefaultCallbacks):
    """
    Class to implement callbacks to measure custom
    metrics for ConservationGame
    """

    # Static method that stores a reference
    # to a prototypical ConservationGame
    game_instance: ConservationGame = None
    game_config: dict = None

    @classmethod
    def set_game_params(
        cls, game_instance: ConservationGame, game_config: dict
    ) -> None:
        cls.game_instance = game_instance
        cls.game_config = game_config
        logging.info(
            f"({cls}) Setting {cls.game_instance=} and {cls.game_config=}"
        )

    #    def on_episode_start(
    #        self,
    #        *,
    #        worker: RolloutWorker,
    #        base_env: BaseEnv,
    #        policies: dict,
    #        episode: Episode,
    #        env_index: int,
    #        **kwargs,
    #    ):  # Episode v2 starts length at -1
    #        assert episode.length == -1, (
    #            f"ERROR: `on_episode_start()` callback should be called right "
    #            f"after env reset! {episode.length=}"
    #        )
    #        # get unwrapped env
    #        env = base_env._unwrapped_env
    #
    #        # initialise metrics for all agents
    #        for poacher in env.poachers:
    #            episode.custom_metrics["num_traps_" + poacher] = []
    #
    #        for agent in env.agents:
    #            metric_name = "location_" + agent
    #            episode.custom_metrics[metric_name] = np.zeros(
    #                [env.grid.N] * 2, dtype=np.int32
    #            )
    #
    #    def on_episode_step(
    #        self,
    #        *,
    #        worker: RolloutWorker,
    #        base_env: BaseEnv,
    #        policies: Dict,
    #        episode: Episode,
    #        env_index: int,
    #        **kwargs,
    #    ):
    #        # Make sure this episode is ongoing.
    #        assert episode.length > 0, (
    #            "ERROR: `on_episode_step()` callback should not be called right "
    #            "after env reset!"
    #        )
    #        # Get base environment
    #        env = base_env._unwrapped_env
    #
    #        # collect data
    #        # First collecting number of traps held by each poacher.
    #        for poacher in env.poachers:
    #            episode.custom_metrics["num_traps_" + poacher].append(
    #                len(env.poacher_traps[poacher])
    #            )
    #        # Update the location of each agent
    #        for agent in env.agents:
    #            # append location to last episode store
    #            loc = tuple(env.grid.state[agent][:2])
    #            episode.custom_metrics["location_" + agent][loc[0], loc[1]] += 1
    #
    #    def on_evaluate_start(
    #        self,
    #        *,
    #        algorithm: Algorithm,
    #        **kwargs,
    #    ) -> None:
    #        """Callback before evaluation starts.
    #        Checkpoint the algorithm before any evaluation,
    #        and save the path to a local folder.
    #        """
    #        algorithm.save()
    #
    def on_evaluate_end(
        self,
        *,
        algorithm: Algorithm,
        evaluation_metrics: dict,
        **kwargs,
    ) -> None:
        """Runs at the end of Algorithm.evaluate().
        Currently used to evaluate exploitability.
        """
        self.policies = (
            ConservationCallbacks.game_instance.unwrapped.possible_agents
        )

        # Get weights from current algorithm
        current_weights = {
            policy: algorithm.get_policy(policy).get_weights()
            for policy in self.policies
        }
        # Setting the weights to use when resetting the new_algos
        RestoreWeightsCallback.set_current_weights(current_weights)

        # To train these algos using current weights
        new_configs = {
            policy: (
                # Get the default algo config
                pg_config(
                    ConservationCallbacks.game_instance,
                    ConservationCallbacks.game_config,
                    policies_to_train=[policy],
                ).evaluation(
                    evaluation_num_workers=1,
                    evaluation_interval=1,
                    always_attach_evaluation_results=True,
                )
                # Callbacks to restore the weights from main algorithm
                .callbacks(RestoreWeightsCallback)
            )
            for policy in self.policies
        }

        rewards_mean = {
            policy: algorithm.evaluation_metrics["evaluation"][
                "policy_reward_mean"
            ][policy]
            for policy in self.policies
        }
        rewards_mean_br = dict.fromkeys(self.policies)
        exploitability_policies = dict.fromkeys(self.policies)

        for policy, new_config in new_configs.items():
            results = tune.Tuner(
                "PG",
                param_space=new_config,
                tune_config=tune.TuneConfig(
                    metric=f"evaluation/policy_reward_mean/{policy}",
                    mode="max",
                    num_samples=1,
                ),
                run_config=air.RunConfig(
                    stop=lambda t_id, res: res["evaluation"][
                        "policy_reward_mean"
                    ][policy]
                    >= rewards_mean[policy]
                    or res["training_iteration"] >= 5,
                    local_dir="./results/",
                    name="exploitability",
                ),
            ).fit()
            expl_results = results.get_best_result().metrics
            # Get the mean_reward for player i BR from the last evaluation iteration
            rewards_mean_br = expl_results["evaluation"]["policy_reward_mean"][
                policy
            ]
            exploitability_policies[policy] = max(
                rewards_mean_br - rewards_mean[policy], 0
            )

        algorithm.evaluation_metrics["evaluation"][
            "br_policy_reward_mean"
        ] = rewards_mean_br
        algorithm.evaluation_metrics["evaluation"][
            "policy_exploitability"
        ] = exploitability_policies
        algorithm.evaluation_metrics["evaluation"]["exploitability"] = sum(
            exploitability_policies.values()
        )
        logging.info(
            f"Exploitability =  {sum(exploitability_policies.values())}"
        )
        tune.report({"exploitability": sum(exploitability_policies.values())})


if __name__ == "__main__":
    args, _ = get_args()
    ray.init(
        log_to_driver=args.verbose,
        num_gpus=args.num_gpus,
        num_cpus=args.num_cpus,
    )

    # call ConservationGame with parser parameters
    # and store a reference to the created game in the callbacks.
    cg, cg_config = define_game_from_args(args)
    ConservationCallbacks.set_game_params(cg, cg_config)

    # build and run
    config = (
        pg_config(cg, cg_config)
        # Custom evaluation parameters
        .evaluation(
            evaluation_num_workers=args.eval_workers,  # Specify number of eval workers in addition to rollout
            evaluation_interval=args.eval_every,
            always_attach_evaluation_results=True,
            evaluation_parallel_to_training=False,
        )
        .resources(num_cpus_per_worker=args.num_cpus_per_worker)
        .reporting(
            # Store custom metrics without calculating max/min/mean
            keep_per_episode_custom_metrics=True
        )
    )

    if args.no_expl is False:
        # Only use callbacks if no-expl is not set.
        config.callbacks(ConservationCallbacks)

    # Using custom reporters to add exploitability scores to output
    custom_metric_reporter = tune.CLIReporter()
    custom_metric_reporter.add_metric_column(
        "evaluation/exploitability", representation="exploitability"
    )

    # using tune to automatically checkpoint
    # to the local directory "results/"
    results = tune.Tuner(
        "PG",
        param_space=config,
        run_config=air.RunConfig(
            stop={
                "training_iteration": args.iters,
                "timesteps_total": 100_000,
            },
            local_dir="./.results/",
            progress_reporter=custom_metric_reporter,
        ),
        tune_config=tune.TuneConfig(
            metric="evaluation/exploitability",
            mode="min",
            reuse_actors=False,
        ),
    ).fit()

    # Shutdown gracefully, saving if required.
    ray.shutdown()

    logging.info("-" * 40)
    logging.info(results)
