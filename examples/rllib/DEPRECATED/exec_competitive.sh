#!/bin/bash

# Defining common flags
declare -i TIMESTEPS=1000000
declare -i EVAL_EVERY=100000
declare -i BATCH_SIZE=2

# Declaring program-specific things
PROG=main.py
PYTHON=python
FLAGS="--use-hyper --verbose --eval-every ${EVAL_EVERY} --timesteps ${TIMESTEPS} --eval-workers 1 --num-cpus 12" 
COMP_FLAGS="--is-competitive"
MEM_FLAGS="--use-memory"

# Process the options
while getopts ":cm" option; do
   case $option in
       c) FLAGS="${FLAGS} ${COMP_FLAGS}" ;;
       m) FLAGS="${FLAGS} ${MEM_FLAGS}" ;;
   esac
done

# Experimental settings
declare -a NUM_AGENTS=( 
	"1 1"
	"2 2"
	"2 4" 
	"4 4"
	"8 8" 
)

# Specify algos as options:
# $ ./exec.sh PPO PG
for ALGO in "$@"
do	
    if [[ ${ALGO} == -* ]]; 
    then
        echo Skipping $ALGO ... ;
	    continue; # Skip command line options
    fi

    N=BATCH_SIZE
    (
    # Variations on number of agents
    for temp in "${NUM_AGENTS}"; do
        ((i=i%N)); ((i++==0)) && wait ;
        read -a num <<< "$temp" && ${PYTHON} ${PROG} ${FLAGS} --algo ${ALGO} --rangers ${num[0]} --poachers ${num[1]} --grid 10 --trial-name ${ALGO}_nr_${num[0]}_np_${num[1]}_grid_10_${exp_type} &
        sleep 10;
    done
    )

    wait;

    # Variations on grid sizes
    ${PYTHON} ${PROG} ${FLAGS} --algo ${ALGO} --grid 15 --trial-name ${ALGO}_nr_2_np_2_grid_15_${exp_type} &
    sleep 10;
    
    # Variations on probability: DT
    ${PYTHON} ${PROG} ${FLAGS} --algo ${ALGO} --grid 10 --prob-trap 0.8 --trial-name ${ALGO}_nr_2_np_2_grid_10_pDT_0_8_${exp_type} &
    sleep 10;
    
    wait;

    # Variations on probability: DP
    ${PYTHON} ${PROG} ${FLAGS} --algo ${ALGO} --grid 10 --prob-cell 0.8 --trial-name ${ALGO}_nr_2_np_2_grid_10_pDP_0_8_${exp_type} &
    sleep 10;

    # Variations on probability: DT
    ${PYTHON} ${PROG} ${FLAGS} --algo ${ALGO} --grid 10 --prob-anim 0.8 --trial-name ${ALGO}_nr_2_np_2_grid_10_pCA_0_8_${exp_type} &
    wait ;
done
