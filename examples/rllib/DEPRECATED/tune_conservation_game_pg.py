import argparse
import numpy as np
from example_utils import get_args, define_game_from_args
from ray.rllib.algorithms.algorithm_config import AlgorithmConfig
from anti_poaching.anti_poaching_game_v0 import AntiPoachingGame
from ray import air, tune
from ray.rllib.policy.policy import PolicySpec
from ray.rllib.algorithms.callbacks import DefaultCallbacks
from ray.rllib.algorithms.pg import PGConfig


class ComputeCustomMetric(DefaultCallbacks):
    def on_train_result(self, *, algorithm, result: dict, **kwargs):
        # Compute sum of the absolute value of multiple policies
        result["custom_metrics"]["policies_absolute_reward_mean"] = sum(
            np.abs(list(result["policy_reward_mean"].values()))
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--stop-iters", type=int, help="Number of iterations to train."
    )
    parser.add_argument(
        "--stop-timesteps", type=int, help="Number of timesteps to train."
    )
    parser.add_argument(
        "--stop-reward", type=float, help="Reward at which we stop training."
    )
    parser.add_argument(
        "--num-samples",
        type=int,
        default=1,
        help="Number of times to sample from the hyperparameter space.If this is -1, (virtually) infinite samples are generated until a stopping condition is met.",
    )
    args = parser.parse_args()
    stop = {}
    if args.stop_iters is not None:
        # Add the parameter to the dictionary
        stop["training_iteration"] = args.stop_iters

    if args.stop_timesteps is not None:
        # Add the parameter to the dictionary
        stop["timesteps_total"] = args.stop_timesteps

    if args.stop_reward is not None:
        # Add the parameter to the dictionary
        stop["episode_reward_mean"] = args.stop_reward

    cg_args, _ = get_args()
    # call AntiPoachingGame with parser parameters
    cg, cg_config = define_game_from_args(cg_args)
    algo = "PG"
    config = (
        PGConfig()
        .environment(AntiPoachingGame.metadata["name"], env_config=cg_config)
        .framework("torch")
        .training(
            train_batch_size=tune.choice([128, 256, 512, 1024, 2048, 4096]),
            gamma=tune.choice([0.9, 0.95, 0.97, 0.99, 0.999]),
            lr=tune.choice([3e-05, 0.0001, 0.0004, 0.001]),
            model={
                "custom_model": "TorchActionMaskModel",
                "custom_model_config": {
                    "no_masking": False
                },  # Required by the example
            },
        )
        .reporting(
            metrics_num_episodes_for_smoothing=1,
        )
        .multi_agent(
            policies={
                agent: PolicySpec(
                    None,  # infer automatically
                    observation_space=cg.observation_space[agent],
                    action_space=cg.action_space[agent],
                )
                for agent in cg.agents
            },
            policy_mapping_fn=lambda aid, episode, worker, **kw: aid,
        )
        .callbacks(ComputeCustomMetric)
        .rollouts(
            rollout_fragment_length="auto",
            # observation_filter=tune.choice(['MeanStdFilter', 'NoFilter']),
        )
    )

    # Create Tuner
    tuner = tune.Tuner(
        algo,
        # Add some parameters to tune
        param_space=config.to_dict(),
        # Specify tuning behavior
        tune_config=tune.TuneConfig(
            metric="custom_metrics/policies_absolute_reward_mean",
            mode="min",
            num_samples=args.num_samples,
        ),
        run_config=air.RunConfig(stop=stop),
    )
    # Run tuning job Tuner.fit() generates a ResultGrid object. This object contains metrics, results, and checkpoints of each trial.
    result_grid = tuner.fit()
    print("result_grid", result_grid)
    print(
        "result_grid._experiment_analysis.trials",
        result_grid._experiment_analysis.trials,
    )
    print("Trial evaluated params")
    for trial in result_grid._experiment_analysis.trials:
        print(trial.evaluated_params)
    # get best trial
    best_trial = result_grid._experiment_analysis.get_best_trial()
    # Get the best result
    best_result = result_grid.get_best_result()
    print("best_result", best_result)
    print("best_trial.config", best_trial.config)
    best_config = best_trial.config
    # Print best hyperparameters
    print("best_trial.evaluated_params", best_trial.evaluated_params)
