"""
Module that implements PPO for the ConservationGame and 
measures custom metrics. Based on the example at 
https://github.com/ray-project/ray/blob/master/rllib/examples/hist_data_and_callbacks.py#L81C1-L87C14

The custom callbacks also include callbacks for before and after calls to Algorithm.evaluate();
This is used to create checkpoints of all policies that are about to be evaluated.

To train on a 2Rx2P instance on a 3x3 grid, the following is suggested.
$ python3 pg_custom_metrics.py --iters 10 --num-cpus 6 --num-cpus-per-worker 1 \
                             --rangers 2 --poachers 2 --grid 5 \
                             --eval-workers 1 --eval-every 5

NOTE: Evaluation with callbacks will use double the CPU requirements of an experiment ! 
      This is because it launches Tune instances internally which will not reuse the 
      CPUs used for training.
"""

# Core modules
import logging

# Ray imports
import ray
from ray import air, tune
from ray.rllib.algorithms.pg import PGConfig

# Importing custom code
from conservation_game.conservation_game_v0 import (
    ConservationGame,
)
from example_utils import (
    get_args,
    define_game_from_args,
    generate_training_config,
)
import callbacks


def pg_config(
    env: ConservationGame,
    env_config: dict = None,
    policies_to_train=None,
    num_gpus: int = 0,
) -> PGConfig:
    """Builds the default PPO Config for ConservationGame"""
    hyperparams = (
        {"gamma": 0.9, "lr": 0.0004, "train_batch_size": 128}
        if args.use_hyper
        else {}
    )
    return generate_training_config(
        game=env,
        game_config=env_config,
        policies_to_train=policies_to_train,
        hyperparams=hyperparams,
        algo_name="PG",
    )


if __name__ == "__main__":
    args, _ = get_args()
    ray.init(
        log_to_driver=args.verbose,
        num_gpus=args.num_gpus,
        num_cpus=args.num_cpus,
    )

    # call ConservationGame with parser parameters
    # and store a reference to the created game in the callbacks.
    cg, cg_config = define_game_from_args(args)

    # build and run
    config = (
        pg_config(cg, cg_config)
        # Custom evaluation parameters
        .evaluation(
            evaluation_num_workers=args.eval_workers,
            evaluation_interval=args.eval_every,
            always_attach_evaluation_results=True,
            evaluation_parallel_to_training=False,
        )
        .resources(num_cpus_per_worker=args.num_cpus_per_worker)
        .reporting(
            # Store custom metrics without calculating max/min/mean
            keep_per_episode_custom_metrics=True
        )
    )

    if args.no_expl is False:
        # Only use callbacks if no-expl is not set.
        # Defining using a lambda to get a partially applied function
        # using the local context
        config.callbacks(
            lambda cg=cg, cg_config=cg_config, config_generator=pg_config: callbacks.CollectMetricsAndExploit(
                game_instance=cg,
                game_config=cg_config,
                new_config_generator=config_generator,
            )
        )

    # Using custom reporters to add exploitability scores to output
    custom_metric_reporter = tune.CLIReporter()
    custom_metric_reporter.add_metric_column(
        "evaluation/exploitability", representation="exploitability"
    )

    # using tune to automatically checkpoint
    # to the local directory "results/"
    results = tune.Tuner(
        "PG",
        param_space=config,
        run_config=air.RunConfig(
            stop={
                "timesteps_total": args.timesteps,
            },
            local_dir="./.results/",
            progress_reporter=custom_metric_reporter,
        ),
        tune_config=tune.TuneConfig(
            metric="evaluation/exploitability",
            mode="min",
            reuse_actors=False,
            trial_name_creator=lambda trial: (
                args.trial_name if args.trial_name else None
            ),
        ),
    ).fit()

    # Shutdown gracefully, saving if required.
    ray.shutdown()

    logging.info("-" * 40)
    logging.info(results)
