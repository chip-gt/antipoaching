"""
Module that implements PPO for the ConservationGame and 
measures custom metrics. Based on the example at 
https://github.com/ray-project/ray/blob/master/rllib/examples/hist_data_and_callbacks.py#L81C1-L87C14

The custom callbacks also include callbacks for before and after calls to Algorithm.evaluate();
This is used to create checkpoints of all policies that are about to be evaluated.

To train on a 2Rx2P instance on a 3x3 grid, the following is suggested.
$ python3 ppo_custom_metrics.py --iters 10 --num-cpus 10 --num-cpus-per-worker 1 \
                             --rangers 2 --poachers 2 --grid 3 \
                             --eval-workers 1 --eval-every 5

NOTE: Evaluation with callbacks will use double the CPU requirements of an experiment ! 
      This is because it launches Tune instances internally which will not reuse the 
      CPUs used for training.

"""

# Core modules
import logging

# Ray imports
import ray
from ray import air, tune
from ray.rllib.algorithms.ppo import PPO

# Importing custom code
from conservation_game.conservation_game_v0 import (
    ConservationGame,
)
from marl_ppo_example import ppo_config
from example_utils import get_args, define_game_from_args
import callbacks as cb

if __name__ == "__main__":
    args, _ = get_args()
    print(args)
    ray.init(
        log_to_driver=args.verbose,
        num_gpus=args.num_gpus,
        num_cpus=args.num_cpus,
    )

    # call ConservationGame with parser parameters
    # and store a reference to the created game in the callbacks.
    cg, cg_config = define_game_from_args(args)

    # build and run
    config = (
        ppo_config(cg, cg_config)
        # Custom evaluation parameters
        # Specify number of eval workers in addition to rollout
        .evaluation(
            evaluation_num_workers=args.eval_workers,
            evaluation_interval=args.eval_every,
            always_attach_evaluation_results=True,
            evaluation_parallel_to_training=True,
        )
        # Register the custom callbacks class
        .callbacks(
            lambda cg=cg, cg_config=cg_config, config_generator=ppo_config: cb.CollectMetricsAndExploit(
                game_instance=cg,
                game_config=cg_config,
                new_config_generator=config_generator,
            )
        )
        .resources(num_cpus_per_worker=args.num_cpus_per_worker)
        .reporting(
            # Store custom metrics without calculating max/min/mean
            keep_per_episode_custom_metrics=True
        )
    )
    if args.no_expl:
        config.callbacks(ConservationCallbacks)

    # Using custom reporters to add exploitability scores to output
    custom_metric_reporter = tune.CLIReporter()
    custom_metric_reporter.add_metric_column(
        "evaluation/exploitability", representation="exploitability"
    )

    # using tune to automatically checkpoint
    # to the local directory "results/"
    results = tune.Tuner(
        PPO,
        param_space=config,
        run_config=air.RunConfig(
            stop={
                "timesteps_total": args.timesteps,
            },
            local_dir="./.results/",
            progress_reporter=custom_metric_reporter,
        ),
        tune_config=tune.TuneConfig(
            metric="evaluation/exploitability",
            mode="min",
            reuse_actors=True,
            trial_name_creator=lambda trial: args.trial_name,
        ),
    ).fit()

    # Shutdown gracefully, saving if required.
    ray.shutdown()

    logging.info("-" * 40)
    logging.info(results)
