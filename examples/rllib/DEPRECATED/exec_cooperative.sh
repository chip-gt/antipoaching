#!/bin/bash

# Defining common flags
declare -i TIMESTEPS=10000
declare -i EVAL_EVERY=2000
declare -i BATCH_SIZE=4

# Declaring program-specific things
PROG=main.py
PYTHON=python3.8
FLAGS="--use-hyper --verbose --eval-every ${EVAL_EVERY} --timesteps ${TIMESTEPS} --eval-workers 1 --num-cpus 20 --policies-train r"
MEM_FLAGS="--use-memory"

# Process the options
while getopts ":cm" option; do
   case $option in
       m) FLAGS="${FLAGS} ${MEM_FLAGS}" ;;
   esac
done

# Experimental settings
declare -a NUM_AGENTS=( 
	"1 1"
	"2 2"
	"2 4" 
	"4 4"
	"8 8" 
)

for ALGO in "$@"
do	
    if [[ ${ALGO} == -* ]]; 
    then
        echo Skipping $ALGO ... ;
	    continue; # Skip command line options
    fi

    for poacher_pol in random greedy
	do 
        # Variations on number of agents
        for temp in "${NUM_AGENTS}"; do
			read -a num <<< "$temp" && ${PYTHON} ${PROG} ${FLAGS} --ppol ${poacher_pol} --algo ${ALGO} --rangers ${num[0]} --poachers ${num[1]} --grid 10 --trial-name ${ALGO}_nr_${num[0]}_np_${num[1]}_grid_10_${poacher_pol} ;
        done

		# Variations on grid sizes
		${PYTHON} ${PROG} ${FLAGS} --ppol ${poacher_pol} --algo ${ALGO} --grid 15 --trial-name ${ALGO}_nr_2_np_2_grid_15_${poacher_pol} ;		
		# Variations on probability: DT
		${PYTHON} ${PROG} ${FLAGS} --ppol ${poacher_pol} --algo ${ALGO} --grid 10 --prob-trap 0.8 --trial-name ${ALGO}_nr_2_np_2_grid_10_pDT_0_8_${poacher_pol} ;
		# Variations on probability: DP
		${PYTHON} ${PROG} ${FLAGS} --ppol ${poacher_pol} --algo ${ALGO} --grid 10 --prob-cell 0.8 --trial-name ${ALGO}_nr_2_np_2_grid_10_pDP_0_8_${poacher_pol} ;
		# Variations on probability: DT
		${PYTHON} ${PROG} ${FLAGS} --ppol ${poacher_pol} --algo ${ALGO} --grid 10 --prob-anim 0.8 --trial-name ${ALGO}_nr_2_np_2_grid_10_pCA_0_8_${poacher_pol} ;
	done
done
