#!/bin/bash

# Defining common flags
declare -i TIMESTEPS=1000000
declare -i EVAL_EVERY=100000

# Declaring program-specific things
PROG=main.py
PYTHON=python
FLAGS="--use-hyper --verbose --eval-every ${EVAL_EVERY} --timesteps ${TIMESTEPS} --eval-workers 2 --num-cpus 20 --rangers 2 --poachers 2 --expl --is-competitive --prob-cell 0.8"
MEM_FLAGS="--use-memory"
PCA_ARGS="--prob-anim 0.2 0.3 --prob-anim-mode kernel"
EXP_TYPE=competitive

for ALGO in "$@"
do	
    # first without memory
    ${PYTHON} ${PROG} ${FLAGS} --algo ${ALGO} --grid 5 --trial-name ${ALGO}_nr_2_np_2_grid_5_nomem_${EXP_TYPE}_PDT_noPCA;
    
    # and then with memory
    ${PYTHON} ${PROG} ${FLAGS} --algo ${ALGO} --grid 5 --trial-name ${ALGO}_nr_2_np_2_grid_5_mem_PDT_noPCA_${EXP_TYPE} ${MEM_FLAGS};

    # Then for kernel mode in pCA
    # first without memory
    ${PYTHON} ${PROG} ${FLAGS} --algo ${ALGO} --grid 5 --trial-name ${ALGO}_nr_2_np_2_grid_5_nomem_PDT_PCAkernel_${EXP_TYPE} ${PCA_ARGS};
    
    # and then with memory
    ${PYTHON} ${PROG} ${FLAGS} --algo ${ALGO} --grid 5 --trial-name ${ALGO}_nr_2_np_2_grid_5_mem_PDT_PCAkernel_${EXP_TYPE} ${MEM_FLAGS} ${PCA_ARGS};
done
