import argparse
import numpy as np
from example_utils import get_args, define_game_from_args
from ray.rllib.algorithms.algorithm_config import AlgorithmConfig
from anti_poaching.anti_poaching_game_v0 import AntiPoachingGame
from ray import air, tune
from ray.rllib.policy.policy import PolicySpec
from ray.rllib.algorithms.callbacks import DefaultCallbacks


class ComputeCustomMetric(DefaultCallbacks):
    def on_train_result(self, *, algorithm, result: dict, **kwargs):
        # Compute sum of the absolute value of multiple policies
        result["custom_metrics"]["policies_absolute_reward_mean"] = sum(
            np.abs(list(result["policy_reward_mean"].values()))
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--stop-iters", type=int, help="Number of iterations to train."
    )
    parser.add_argument(
        "--stop-timesteps", type=int, help="Number of timesteps to train."
    )
    parser.add_argument(
        "--stop-reward", type=float, help="Reward at which we stop training."
    )
    parser.add_argument(
        "--num-samples",
        type=int,
        default=1,
        help=(
            "Number of times that we run with the same hyperparameter with different seeds."
        ),
    )
    parser.add_argument(
        "--evaluation-interval",
        type=int,
        default=1,
        help="Interval of training iterations for which we will run evaluation. E.g. we will run the evaluation iteration every [evaluation_interval] training training_iterations",
    )

    args = parser.parse_args()
    stop = {}
    if args.stop_iters is not None:
        # Add the parameter to the dictionary
        stop["training_iteration"] = args.stop_iters

    if args.stop_timesteps is not None:
        # Add the parameter to the dictionary
        stop["timesteps_total"] = args.stop_timesteps

    if args.stop_reward is not None:
        # Add the parameter to the dictionary
        stop["episode_reward_mean"] = args.stop_reward

    cg_args, _ = get_args()
    # call AntiPoachingGame with parser parameters
    cg, cg_config = define_game_from_args(cg_args)
    algo = "PPO"
    config = (
        AlgorithmConfig(algo_class=algo)
        .environment(AntiPoachingGame.metadata["name"], env_config=cg_config)
        .framework("torch")
        .training(
            train_batch_size=4000,
            lr=5e-5,
            model={
                "custom_model": "TorchActionMaskModel",
                "custom_model_config": {
                    "no_masking": False
                },  # Required by the example
            },
        )
        .evaluation(
            evaluation_num_workers=1,
            evaluation_interval=args.evaluation_interval,
            always_attach_evaluation_results=True,
        )
        .rollouts(rollout_fragment_length="auto")
        .reporting(
            metrics_num_episodes_for_smoothing=1,
        )
        .multi_agent(
            policies={
                agent: PolicySpec(
                    None,  # infer automatically
                    observation_space=cg.observation_space[agent],
                    action_space=cg.action_space[agent],
                )
                for agent in cg.agents
            },
            policy_mapping_fn=lambda aid, episode, worker, **kw: aid,
        )
        .callbacks(ComputeCustomMetric)
    )
    exp_name = "conservation_game" if not args.trial_name else args.trial_name
    # Create Tuner
    tuner = tune.Tuner(
        algo,
        # Add some parameters to tune
        param_space=config.to_dict(),
        # Specify tuning behavior
        tune_config=tune.TuneConfig(
            metric="episode_reward_mean",
            mode="max",
            num_samples=args.num_samples,
        ),
        run_config=air.RunConfig(
            stop=stop, local_dir="./results/", name=exp_name
        ),
    )
    # Run tuning job Tuner.fit() generates a ResultGrid object. This object contains metrics, results, and checkpoints of each trial.
    result_grid = tuner.fit()
    # Check if there have been errors
    if result_grid.errors:
        print("At least one trial failed.")
