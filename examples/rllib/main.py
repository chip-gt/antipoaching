"""
Main module that implements PPO/PG/QMix for the AntiPoachingGame and optionally
measures custom metrics. Based on the example at 
(hist_data_and_callbacks.py)[https://github.com/ray-project/ray/blob/master/rllib/examples/hist_data_and_callbacks.py#L81C1-L87C14]

The custom callbacks also include callbacks for before and after calls to Algorithm.evaluate();
This is used to create checkpoints of all policies that are about to be evaluated.

To train on a 2Rx4P instance on a 15x15 grid, the following is suggested.
 .. highlight:: bash
 .. code-block:: bash

    $ python3 main.py --num-cpus 10 --num-cpus-per-worker 1 --eval-workers 1 \ 
              --timesteps 30000 --eval-every 10000 --rangers 2 --poachers 4 --grid 15 \

To train only the rangers and have the poachers use a Random strategy
 .. highlight:: bash
 .. code-block:: bash

    $ python3 main.py --policies_train r --ppol random 

**NOTE**: 
Evaluation with :meth:`callbacks.ExploitabilityCallback` will multiply resource
consumption by the number of agents. This is because it launches Tune Trials internally 
to create fixed learning environments for each agent.
"""

# Core modules
import json
import pathlib
import functools

# Ray imports
import ray
import torch
from ray import air, tune
from ray.rllib.algorithms.ppo import PPO, PPOConfig
from ray.rllib.algorithms.qmix import QMixConfig, QMix
from ray.rllib.algorithms.maddpg import MADDPGConfig, MADDPG
from ray.rllib.algorithms.callbacks import make_multi_callbacks

# Importing custom code
from anti_poaching.anti_poaching_v0 import anti_poaching
from heuristic_policies import (
    ActionMaskedRandomPolicy,
    ActionMaskedStaticPoacherPolicy,
    QMIXActionMaskedRandomPolicy,
    QMIXGreedyStaticPolicy,
)
from configs import (
    ppo_config,
    pg_config,
    qmix_config,
    duplicator,
    generic_conf,
    _TRAIN_BATCH_SIZE,
)
from example_utils import get_args, define_game_from_args
import callbacks as cb
from recurrent import LSTMActionMaskingModel, QmixLSTMModel


def get_policies_from_arg(pol: str, algo: str):
    """Map agent to Policy Classes"""
    if pol == "random":
        if algo == "QMIX":
            return QMIXActionMaskedRandomPolicy
        else:
            return ActionMaskedRandomPolicy
    elif pol == "static":
        if algo == "QMIX":
            return QMIXGreedyStaticPolicy
        else:
            return ActionMaskedStaticPoacherPolicy
    elif pol == None:
        return None


if __name__ == "__main__":
    args, _ = get_args()
    print(args)
    ray.init(
        configure_logging=True,
        logging_level="error",
        log_to_driver=args.verbose,
        num_gpus=args.num_gpus,
        num_cpus=args.num_cpus,
    )

    # call AntiPoachingGame with parser parameters
    # and store a reference to the created env in the callbacks.
    cg, cg_config = define_game_from_args(args)

    # Choose the agents that learn
    policies_to_train = []
    if "r" in args.policies_train:
        policies_to_train += ["rangers"] if args.algo == "QMIX" else cg.rangers
    if "p" in args.policies_train:
        policies_to_train += (
            ["poachers"] if args.algo == "QMIX" else cg.poachers
        )

    # Choose the algo that runs
    if args.algo == "PPO":
        algo_config = ppo_config
    elif args.algo == "PG":
        algo_config = pg_config
    elif args.algo == "QMIX":
        algo_config = qmix_config
    else:
        raise RuntimeError(f"Unknown algorithm specified: {args.algo}")

    # Agent specific policies
    policy_class_selector = lambda agent: (
        get_policies_from_arg(args.rpol, args.algo)
        if "ranger" in agent
        else get_policies_from_arg(args.ppol, args.algo)
    )

    # build and run chosen algo
    config = (
        algo_config(
            env=cg,
            env_config=cg_config,
            policies_to_train=policies_to_train,
            policy_class_selector=policy_class_selector,
            use_hyper=args.use_hyper,
            use_memory=args.use_memory,
            is_competitive=args.is_competitive,
            num_gpus=args.num_gpus,  # AlgoConfig to use GPUs.
        )
        # Custom evaluation parameters
        # Specify number of eval workers in addition to rollout
        .evaluation(
            evaluation_duration=100,
            always_attach_evaluation_results=True,
            evaluation_num_workers=args.eval_workers,
        )
        .resources(num_cpus_per_worker=args.num_cpus_per_worker)
        .reporting(
            # Store custom metrics without calculating max/min/mean
            keep_per_episode_custom_metrics=True
        )
    )

    # Need to update evaluation frequency based on batch size.
    EVAL_INTERVAL = max(1, args.eval_every // config.get("train_batch_size"))
    config.update_from_dict({"evaluation_interval": EVAL_INTERVAL})

    # Using custom reporters and tune configs to add new metrics
    tune_config_params = {
        "reuse_actors": True,
        "trial_name_creator": lambda trial: args.trial_name or None,
    }

    # If exploitability metric is requested ...
    # Or if the competitive scenario is chosen ...
    if args.expl or args.is_competitive:
        callbacks_list.append(
            lambda cg=cg, cg_config=cg_config: cb.ExploitabilityCallback(
                env_instance=cg,
                env_config=cg_config,
                new_config_generator=duplicator(config),
                _expl_num_iters=100,  # BR will use 100 iters of training.
            )
        )

    # if using the LSTM
    if args.use_memory:
        is_qmix = "QMIX" in args.algo
        config.training(
            model={
                "custom_model": (
                    LSTMActionMaskingModel if not is_qmix else QmixLSTMModel
                ),
                "custom_model_config": {"no_masking": False},
            },
        )
        if not is_qmix:
            config.update_from_dict({"shuffle_sequences": False})

    # using tune to automatically checkpoint
    # to the local directory "results/"
    results = tune.Tuner(
        args.algo,
        param_space=config,
        tune_config=tune.TuneConfig(**tune_config_params),
        run_config=air.RunConfig(
            stop={
                "timesteps_total": args.timesteps,
            },
            # trial_path_generator avoids race conditions which
            # might occur when two trials save to the same folder
            storage_path=pathlib.Path(f".results/").resolve(),
            checkpoint_config=air.CheckpointConfig(
                checkpoint_at_end=True,
                checkpoint_frequency=EVAL_INTERVAL,
            ),
        ),
    ).fit()

    # Get the path to this experiment and save env_config there.
    env_config_file_path = pathlib.Path(results[0].path) / "env_config.json"
    if "grid_class" in cg_config:
        # Store only the name of the class.
        cg_config["grid_class"] = cg_config["grid_class"].__name__
    with open(env_config_file_path, "w") as fd:
        print(json.dumps(cg_config), file=fd)

    # Shutdown gracefully, saving if required.
    ray.shutdown()
