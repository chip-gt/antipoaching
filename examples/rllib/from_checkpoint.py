"""
Module that reads the checkpoint from an earlier run and visualises it.
This checks the file "env_config.json" created by `main.py` to recreate the
environment used to train this particular algorithm instance.

For example, to launch from the checkpoint
 .. highlight:: bash
 .. code-block:: bash

    CHPOINT_DIR=.results/PPO/PPOTrial/checkpoint_000010

we can simply use

 .. highlight:: bash
 .. code-block:: bash

    $ python from_checkpoint.py <CHPOINT_DIR>

Note that `CHPOINT_DIR` must point to the algorithm checkpoint directory,
and not the policy checkpoint of an agent. Note also that you must use the same
version of Python used to run the experiment (and thus create the checkpoint). 
This is because `cloudpickle` is used internally by :meth:`Algorithm.from_checkpoint`,
and is not guaranteed to be compatible between Python versions.
"""

import json
import pathlib
import sys
import numpy as np
import ray
from ray.rllib.algorithms.algorithm import Algorithm
from ray.rllib.algorithms.qmix import QMixConfig, QMix

from anti_poaching.anti_poaching_v0 import (
    anti_poaching,
    GridStateVaryingPCA,
    GridStateConstProb,
)


def get_actions(
    algo: Algorithm,
    obs: dict,
    prev_action: dict = None,
    prev_reward: dict = None,
) -> list:
    """Get the actions from the user. This handles the output from
    :meth:`Algorithm.compute_single_action`, and returns only
    the action."""

    agents = obs.keys()
    actions = dict.fromkeys(agents)
    # If no previous action, assign as null action = 0.
    if prev_action == None:
        prev_action = dict.fromkeys(agents, 0)
    if prev_reward == None:
        prev_reward = dict.fromkeys(agents, 0)

    for idx, agent in enumerate(agents):
        timestep = (
            obs[agent][0]["obs"][0]
            if isinstance(algo, QMix)
            else obs[agent]["observations"][0]
        )
        _policy = algo.get_policy(agent)
        actions[agent] = _policy.compute_single_action(
            obs=obs[agent],
            prev_action=prev_action[agent],
            prev_reward=prev_reward[agent],
        )[0]

    return actions


if __name__ == "__main__":
    ray.init(log_to_driver=True)
    assert (
        len(sys.argv) == 2
    ), "Insufficient/Too many checkpoints: provide only one."

    # Get the paths to reinstate algo and env
    checkpoint_path = pathlib.Path(sys.argv[1])
    env_config_path = checkpoint_path.parent / "env_config.json"

    # Set a few configuration bits
    is_qmix: bool = "QMIX" in checkpoint_path.as_posix()

    # Retrieve the environment config used for this experiment.
    with open(env_config_path, "r") as fd:
        env_config = json.load(fd)
    env_config["render_mode"] = "rgb"

    # Reinstate algo and create env for now
    # Recall that QMix uses the GroupAgentsWrapper for any env.
    algo = Algorithm.from_checkpoint(checkpoint_path, policies_to_train=[])
    if "grid_class" in env_config:
        env_config["grid_class"] = getattr(
            sys.modules[__name__], env_config["grid_class"]
        )  # Convert string to class handle
    env = algo.env_creator(env_config)
    base_env = (
        env.env.get_sub_environments if is_qmix else env.get_sub_environments
    )

    # Check if using a recurrent model, and prev actions/rewards
    is_recurrent = "LSTM" in getattr(
        algo.config.model["custom_model"],
        "__name__",
        algo.config.model["custom_model"],  # Return by default
    )
    use_prev_action = (
        is_recurrent and algo.config.model["lstm_use_prev_action"]
    )
    use_prev_reward = (
        is_recurrent and algo.config.model["lstm_use_prev_reward"]
    )

    # The calm before the storm ...
    obs, info = env.reset()
    done = False
    prev_action, prev_reward = None, None

    # Main execution loop i.e. the storm
    while not done:
        _other_args = {
            "prev_action": prev_action if use_prev_action else None,
            "prev_reward": prev_reward if use_prev_reward else None,
        }
        actions = get_actions(algo, obs, **_other_args)
        obs, reward, terminated, truncated, info = env.step(actions)
        base_env.render()
        done = terminated["__all__"] or truncated["__all__"]
        if is_recurrent:
            prev_action, prev_reward = actions, reward

    # stop and quit
    algo.stop()
    ray.shutdown()
