""""Defines utils for the examples, like argument parsers, and a method to 
define an RLlib ready instance using args."""

import pathlib
import argparse
import json
import datetime as dt
from anti_poaching.anti_poaching_v0 import anti_poaching, GridStateVaryingPCA


def get_args() -> argparse.ArgumentParser:
    """Constructs the arg parser to use CPU/GPU resources"""
    parser = argparse.ArgumentParser()
    args_dir = pathlib.Path(__file__).parent.resolve() / "arguments.json"
    with open(args_dir, "r") as fd:
        arguments = json.load(fd)

    for argname, argparams in arguments.items():
        if "type" in argparams:
            argparams["type"] = int if argparams["type"] == "int" else float
        parser.add_argument(argname, **argparams)
    return parser.parse_known_args()


def define_game_from_args(args: argparse.Namespace) -> tuple:
    """Define a AntiPoachingGame using the specified arguments"""
    cg_config = {
        "grid_size": args.grid,
        "nrangers": args.rangers,
        "npoachers": args.poachers,
        "ntraps_per_poacher": args.ntraps,
        "prob_detect_cell": args.prob_cell,
        "prob_detect_trap": args.prob_trap,
        "prob_animal_appear": args.prob_anim,
        "max_time": args.max_time,
        "seed": args.seed,
    }
    if isinstance(args.prob_anim, list):
        cg_config["grid_class"] = GridStateVaryingPCA
        cg_config["pca_mode"] = args.prob_anim_mode
    return anti_poaching.parallel_env(**cg_config), cg_config


def trial_path_generator(algo: str) -> str:
    """Defines a path to store the trial. This is because running multiple
    trials in the same path is a race condition that crashes trials."""
    return f"{algo}-{dt.datetime.now().strftime('%d-%m-%y-%Hh-%Mmin-%Ss-%fμ')}"
