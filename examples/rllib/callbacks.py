"""
This file contains all the callbacks that will be used 
in the RLlib-based examples. Notably, this includes the 
:class:`ExploitabilityCallback`, which calculates the
Approximate Exploitability for all learning agents in an
RLlib-compatible environment.
"""

# Core modules
import pathlib
import logging

# Core libraries
import numpy as np
import gymnasium as gym

# Ray imports
import ray
from ray import air, tune, train
from ray.tune.logger import pretty_print
from ray.rllib.algorithms.algorithm import Algorithm

# For typing information
from typing import Dict, Tuple, Callable
from ray.rllib.env import BaseEnv
from ray.rllib.policy.policy import PolicySpec
from ray.rllib.evaluation import Episode, RolloutWorker
from ray.rllib.algorithms.callbacks import DefaultCallbacks
from typing import TYPE_CHECKING, Any, Dict, List, Optional, Tuple


class SumRangerRewardMetric(DefaultCallbacks):
    """Callback to compute a custom metric: the sum of all ranger
    rewards. This is stored as a custom metric: rangers_mean_reward_sum"""

    def on_train_result(self, *, algorithm, result: dict, **kwargs):
        result["custom_metrics"]["rangers_mean_reward_sum"] = np.sum(
            [
                v
                for k, v in result["policy_reward_mean"].items()
                if "ranger" in k
            ]
        )


class RestoreNonTrainingAgents(DefaultCallbacks):
    """
    Restores the weights of an algorithm on init
    to the supplied `current_policies` parameter.
    However, this will default the policies that are
    being trained on this config, so this will fix
    non-training agents only.
    """

    def __init__(self, current_policies: list) -> None:
        self.current_policies = current_policies

    def on_algorithm_init(self, *, algorithm: Algorithm, **kwargs) -> None:
        """Reset the weights on algorithm init"""
        assert (
            self.current_policies is not None
        ), "Trying to set empty weights to {algorithm=}"
        to_default = algorithm.get_config().to_dict()["policies_to_train"]

        # Now, replace those policies by default learners with the right spaces.
        for policy_id in to_default:
            curr_policy = algorithm.get_policy(policy_id)
            algorithm.remove_policy(policy_id=policy_id)
            algorithm.add_policy(
                policy_id=policy_id,
                policy_cls=algorithm.get_default_policy_class(
                    algorithm.get_default_config()
                ),
                observation_space=curr_policy.observation_space,
                # self.current_policies[
                #    policy_id
                # ].observation_space,
                action_space=curr_policy.action_space,
                # self.current_policies[policy_id].action_space,
            )
            if policy_id in self.current_policies:
                # Make sure we do not accidentally set those values
                self.current_policies.remove(policy_id)

        # Get the weights and restore for the algorithm
        current_weights = {
            policy_id: algorithm.get_policy(policy_id).get_weights()
            for policy_id in self.current_policies
        }
        algorithm.set_weights(current_weights)


class EpisodeMetricsCallbacks(DefaultCallbacks):
    """Callbacks to collect metrics from
    the Conservation Game"""

    def on_episode_start(
        self,
        *,
        worker: RolloutWorker,
        base_env: BaseEnv,
        policies: dict,
        episode: Episode,
        env_index: int,
        **kwargs,
    ):  # Episode v2 starts length at -1
        assert episode.length == -1, (
            f"ERROR: `on_episode_start()` callback should be called right "
            f"after env reset! {episode.length=}"
        )
        # get unwrapped env
        env = base_env._unwrapped_env

        # initialise metrics for all agents
        for poacher in env.poachers:
            episode.custom_metrics["num_traps_" + poacher] = []

        for agent in env.agents:
            metric_name = "location_" + agent
            episode.custom_metrics[metric_name] = np.zeros(
                [env.grid.N] * 2, dtype=np.int32
            )

    def on_episode_step(
        self,
        *,
        worker: RolloutWorker,
        base_env: BaseEnv,
        policies: Dict,
        episode: Episode,
        env_index: int,
        **kwargs,
    ):
        # Make sure this episode is ongoing.
        assert episode.length > 0, (
            "ERROR: `on_episode_step()` callback should not be called right "
            "after env reset!"
        )
        # Get base environment
        env = base_env._unwrapped_env

        # collect data
        # First collecting number of traps held by each poacher.
        for poacher in env.poachers:
            episode.custom_metrics["num_traps_" + poacher].append(
                len(env.poacher_traps[poacher])
            )
        # Update the location of each agent
        for agent in env.agents:
            # append location to last episode store
            loc = tuple(env.grid.state[agent][:2])
            episode.custom_metrics["location_" + agent][loc[0], loc[1]] += 1


class EvaluationSaveCallback(DefaultCallbacks):
    """Callback to save before every evaluation"""

    def on_evaluate_start(
        self,
        *,
        algorithm: Algorithm,
        **kwargs,
    ) -> None:
        """Callback before evaluation starts.
        Checkpoint the algorithm before any evaluation,
        and save the path to a local folder.
        """
        algorithm.save()


class ExploitabilityCallback(DefaultCallbacks):
    """Class to implement exploitability calculation
    as a Custom callback
    """

    def __init__(
        self,
        env_instance: "AntiPoachingGame" = None,
        env_config: dict = None,
        new_config_generator: Callable = None,
        policies: list = None,
        _expl_num_iters: int = 50,
        _expl_eval_duration: int = 100,
    ) -> None:
        self.env_instance = env_instance
        self.env_config = env_config
        self.new_config_generator = new_config_generator
        self._expl_num_iters = _expl_num_iters
        self._expl_eval_duration = _expl_eval_duration
        self.policies = policies
        logging.info(
            f"({self}) Setting {self.env_instance=} and {self.env_config=}"
        )

    def on_evaluate_end(
        self,
        *,
        algorithm: Algorithm,
        evaluation_metrics: dict,
        **kwargs,
    ) -> None:
        """Runs at the end of Algorithm.evaluate().
        Currently used to evaluate exploitability."""
        if not self.policies:
            # If policies which exploit not set, infer.
            if algorithm.config.policies_to_train:
                self.policies = algorithm.config.policies_to_train
            else:
                self.policies = algorithm.config.policies.keys()

        # Get weights from current algorithm, and build the tune config
        trial_config = (
            self.new_config_generator()
            .callbacks(lambda: RestoreNonTrainingAgents(self.policies))
            .update_from_dict(
                {
                    "evaluation_num_workers": 1,
                    "evaluation_interval": self._expl_num_iters,
                    "evaluation_duration": self._expl_eval_duration,
                    "always_attach_evaluation_results": True,
                    "policies_to_train": tune.grid_search(
                        [[pol] for pol in self.policies]
                    ),  # Using tune to parallelise the calculation part.
                }
            )
        )

        rewards_mean = {
            policy: algorithm.evaluation_metrics["evaluation"][
                "policy_reward_mean"
            ][policy]
            for policy in self.policies
        }
        rewards_mean_br = dict.fromkeys(self.policies)
        exploitability_policies = dict.fromkeys(self.policies)

        # Execute the trials in parallel :) (1 per agent)
        results = tune.Tuner(
            type(algorithm),
            param_space=trial_config,
            run_config=air.RunConfig(
                stop={"training_iteration": self._expl_num_iters},
                storage_path=pathlib.Path("./.results/").resolve(),
                name="exploitability",
            ),
        ).fit()

        # Post-processing the results.
        for result in results:
            policy = list(result.metrics["info"]["learner"].keys())[0]
            rewards_mean_br = result.metrics["evaluation"][
                "policy_reward_mean"
            ][policy]
            exploitability_policies[policy] = max(
                rewards_mean_br - rewards_mean[policy], 0
            )
            algorithm.evaluation_metrics[f"br_{policy}_reward_mean"] = (
                rewards_mean_br
            )

        # calculate the exploitability with the normalisation.
        expl_value = sum(exploitability_policies.values()) / len(
            exploitability_policies
        )
        evaluation_metrics["policy_exploitability"] = exploitability_policies
        evaluation_metrics["exploitability"] = expl_value
        train.report({"exploitability": expl_value})


class CollectMetricsAndExploit(
    ExploitabilityCallback, EpisodeMetricsCallbacks
):
    def __init__(
        self,
        env_instance: "AntiPoachingGame" = None,
        env_config: dict = None,
        new_config_generator: Callable = None,
    ) -> None:
        super(CollectMetricsAndExploit, self).__init__(
            env_instance=env_instance,
            env_config=env_config,
            new_config_generator=new_config_generator,
        )
