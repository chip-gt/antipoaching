"""
This module implements Recurrent neural network models for
PG/PPO/QMix. The hidden layers of each model are dense layers followed 
by an RNN. It is possible to set each RNN model to train on previous actions
and previous rewards, by setting the `use_prev_action` and `use_prev_reward`
in the respective `AlgorithmConfig`.

We implement the generic (PPO/PG) model in :class:`LSTMActionMaskingModel`
and the QMix version in :class:`QmixLSTMModel`.
"""

import torch
import gymnasium as gym
from torch import nn
from typing import Dict, List, Union, Tuple
from ray.rllib.utils.typing import ModelConfigDict, TensorType
from ray.rllib.utils.annotations import override
from ray.rllib.models.modelv2 import ModelV2
from ray.rllib.models.preprocessors import get_preprocessor
from ray.rllib.models.torch.torch_modelv2 import TorchModelV2
from ray.rllib.utils.torch_utils import FLOAT_MIN
from ray.rllib.models.torch.recurrent_net import RecurrentNetwork
from ray.rllib.policy.rnn_sequencing import add_time_dimension
from ray.rllib.policy.sample_batch import SampleBatch
from ray.rllib.policy.view_requirement import ViewRequirement
from ray.util.debug import log_once
from ray.rllib.utils.deprecation import deprecation_warning
from ray.rllib.models.torch.misc import SlimFC


class QmixLSTMModel(TorchModelV2, nn.Module):
    """The default RNN model for QMIX - now with an LSTM. This mirrors the
    LSTMActionMaskingModel with QMIX specific quirks. Notably, QMIX handles
    action masking at the policy level, so we don't do it here."""

    def __init__(
        self,
        obs_space,
        action_space,
        num_outputs,
        model_config,
        name,
        hiddens: list = None,
    ):
        TorchModelV2.__init__(
            self, obs_space, action_space, num_outputs, model_config, name
        )
        nn.Module.__init__(self)

        # using rllib tooling to get the shape
        orig_space = model_config.get("full_obs_space", obs_space)
        num_inputs = get_preprocessor(orig_space["obs"])(
            orig_space["obs"]
        ).size
        self.act_size = get_preprocessor(action_space)(action_space).size

        # Get model params
        self.n_agents = model_config["n_agents"]
        self.use_prev_action = model_config.get("lstm_use_prev_action", False)
        self.use_prev_reward = model_config.get("lstm_use_prev_reward", False)
        self.no_masking = model_config["custom_model_config"].get(
            "no_masking", False
        )

        # Get network parameters
        # self.fcnet_hiddens = model_config["fcnet_hiddens"]  # type: list[int]
        self.lstm_state_size = model_config["lstm_cell_size"]
        hiddens = [256, 256, self.lstm_state_size] if not hiddens else hiddens
        activation = model_config.get("fcnet_activation", "tanh")
        post_activation = model_config.get("post_fcnet_activation", "relu")

        # Specify the core network
        layers, prev_layer_size = [], num_inputs
        for hidden_dim in hiddens:
            layers.append(
                SlimFC(prev_layer_size, hidden_dim, activation_fn=activation)
            )
            prev_layer_size = hidden_dim
        layers.append(
            SlimFC(
                prev_layer_size,
                self.lstm_state_size,
                activation_fn=post_activation,
            )
        )
        self.fcnet = nn.Sequential(*layers)
        self.rnn = nn.GRUCell(self.lstm_state_size, self.lstm_state_size)

        # Specify action and value branches
        self.action_branch = nn.Linear(self.lstm_state_size, self.act_size)
        self.value_branch = nn.Linear(self.lstm_state_size, 1)

        self._features = None  # For the last output

    @override(TorchModelV2)
    def get_initial_state(self):
        return [torch.zeros(self.n_agents, self.lstm_state_size)]

    @override(TorchModelV2)
    def forward(self, inputs, hidden_state, seq_lens):
        x = nn.functional.relu(self.fcnet(inputs["obs"]))
        h_in = hidden_state[0].reshape(-1, self.lstm_state_size)
        h = self.rnn(x, h_in)
        logits = nn.functional.softmax(self.action_branch(h), dim=1)
        return logits, [h]

    @override(TorchModelV2)
    def value_function(self):
        assert self._features is not None, "must call forward() first"
        return self.value_branch(self._features).squeeze(1)


class LSTMActionMaskingModel(RecurrentNetwork, nn.Module):
    def __init__(
        self,
        obs_space,
        action_space,
        num_outputs,
        model_config,
        name,
        lstm_state_size=256,
    ):
        nn.Module.__init__(self)
        super().__init__(
            obs_space, action_space, num_outputs, model_config, name
        )

        # Get the original space size
        orig_space = getattr(obs_space, "original_space", obs_space)
        num_inputs = get_preprocessor(orig_space["observations"])(
            orig_space["observations"]
        ).size

        # Get input modifiers from model_config
        self.model_config = model_config
        self.use_prev_action = model_config.get("lstm_use_prev_action", False)
        self.use_prev_reward = model_config.get("lstm_use_prev_reward", False)
        self.no_masking = model_config["custom_model_config"].get(
            "no_masking", False
        )

        # Update ViewRequirements and num_inputs for prev actions/rewards
        self.view_requirements["actions"] = ViewRequirement(
            shift=0,
            data_col="actions",
            space=self.action_space,
        )
        if self.use_prev_action:
            self.view_requirements["prev_actions"] = ViewRequirement(
                shift=-1,
                data_col="actions",
                space=self.action_space,
            )
            num_inputs += 1
        if self.use_prev_reward:
            self.view_requirements["prev_rewards"] = ViewRequirement(
                shift=-1, data_col="rewards"
            )
            num_inputs += 1

        # Get other shape parameters
        act_size = get_preprocessor(action_space)(action_space).size
        self.lstm_state_size = lstm_state_size
        hiddens = list(model_config.get("fcnet_hiddens", [])) + list(
            model_config.get("post_fcnet_hiddens", [])
        )

        # Defining the linear/dense model layers
        activation = model_config.get("fcnet_activation")
        layers, prev_layer_size = [], num_inputs

        # Adding the hiddens
        for size in hiddens:
            layers.append(
                SlimFC(
                    in_size=prev_layer_size,
                    out_size=size,
                    activation_fn=activation,
                )
            )
            prev_layer_size = size

        # and then the final Linear layer
        layers.append(
            SlimFC(
                in_size=prev_layer_size,
                out_size=self.lstm_state_size,
                activation_fn=model_config.get("post_fcnet_activation"),
            )
        )

        # Build the Module from fc + LSTM + 2xfc (action + value outs).
        self.fcnet = nn.Sequential(*layers)
        self.lstm = nn.LSTM(
            hiddens[-1], self.lstm_state_size, batch_first=True
        )
        self.action_branch = nn.Linear(self.lstm_state_size, num_outputs)
        self.value_branch = nn.Linear(self.lstm_state_size, 1)

        # Holds the current "base" output (before logits layer).
        self._features = None

    def get_initial_state(self):
        return [
            torch.zeros(1, self.lstm_state_size).squeeze(0),
            torch.zeros(1, self.lstm_state_size).squeeze(0),
        ]

    def value_function(self):
        assert self._features is not None, "must call forward() first"
        return torch.reshape(self.value_branch(self._features), [-1])

    def forward(
        self,
        input_dict: Dict[str, TensorType],
        state: List[TensorType],
        seq_lens: TensorType,
    ) -> Tuple[TensorType, List[TensorType]]:
        # Creating a __init__ function that acts as a passthrough and adding the warning
        # there led to errors probably due to the multiple inheritance. We encountered
        # the same error if we add the Deprecated decorator. We therefore add the
        # deprecation warning here.
        if log_once("recurrent_network_tf"):
            deprecation_warning(
                old="ray.rllib.models.torch.recurrent_net.RecurrentNetwork"
            )
        flat_inputs = input_dict["obs"]["observations"].float()
        action_mask = input_dict["obs"]["action_mask"].float()

        # add the previous actions + rewards if requested.
        if self.use_prev_action:
            flat_inputs = torch.cat(
                [flat_inputs, input_dict["prev_actions"].unsqueeze(1)], dim=1
            )
        if self.use_prev_reward:
            flat_inputs = torch.cat(
                [flat_inputs, input_dict["prev_rewards"].unsqueeze(1)], dim=1
            )
        if not isinstance(seq_lens, torch.Tensor) and not seq_lens:
            # This happens when calling compute_single_action
            seq_lens = torch.ones([flat_inputs.shape[0], 1])
        if not state:
            # This also happens when calling compute_single_action
            state = [torch.unsqueeze(s, 0) for s in self.get_initial_state()]

        # Note that max_seq_len != input_dict.max_seq_len != seq_lens.max()
        # as input_dict may have extra zero-padding beyond seq_lens.max().
        # Use add_time_dimension to handle this.
        self.time_major = self.model_config.get("_time_major", False)
        inputs = add_time_dimension(
            flat_inputs,
            seq_lens=seq_lens,
            framework="torch",
            time_major=self.time_major,
        )
        logits, new_state = self.forward_rnn(inputs, state, seq_lens)
        logits = torch.reshape(logits, [-1, self.num_outputs])

        # Convert action_mask into a [0.0 || -inf]-type mask.
        inf_mask = torch.clamp(torch.log(action_mask), min=FLOAT_MIN)
        masked_logits = logits + inf_mask

        return masked_logits, new_state

    def forward_rnn(self, inputs, state, seq_lens):
        """Feeds `inputs` (B x T x ..) through the Gru Unit.
        Returns the resulting outputs as a sequence (B x T x ...).
        Values are stored in self._features in simple (B) shape (where B
        contains both the B and T dims!).
        Returns:
            NN Outputs (B x T x ...) as sequence.
            The state batches as a List of two items (c- and h-states).
        """
        x = nn.functional.relu(self.fcnet(inputs))
        self._features, [h, c] = self.lstm(
            x, [torch.unsqueeze(state[0], 0), torch.unsqueeze(state[1], 0)]
        )
        action_out = self.action_branch(self._features)
        return action_out, [torch.squeeze(h, 0), torch.squeeze(c, 0)]
