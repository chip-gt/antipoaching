"""
This module holds functions that generate training configs for Algorithms
in RLlib for APE. This currently supports PPO (:func:`ppo_config`),
PG(:func:`pg_config`) and QMix (:func:`qmix_config`). All functions overload the
generic :func:`generate_training_config` which takes care of generic parameters
such as loading the environment and creating policies for each agent.
"""

import json
import copy
import functools
from typing import Callable

import gymnasium as gym
from ray.rllib.algorithms import AlgorithmConfig
from ray.rllib.algorithms.ppo import PPOConfig
from ray.rllib.algorithms.pg import PGConfig
from ray.rllib.algorithms.qmix import QMixConfig
from ray.rllib.policy.policy import PolicySpec
from ray.rllib.env.wrappers.group_agents_wrapper import GroupAgentsWrapper

# Importing my enviroment
from anti_poaching.anti_poaching_v0 import (
    anti_poaching,
    grouped_rllib_cons_game,
)

# Constant batch size for all algorithms
_NUM_EPISODES_PER_ITER = 5
_LEN_EPISODE = 200  # Assuming default.
_TRAIN_BATCH_SIZE = _LEN_EPISODE * _NUM_EPISODES_PER_ITER


def read_hyperparams_for_algo(
    algo: str, is_competitive: bool, use_memory: bool, use_hyper: bool
) -> dict:
    # _TRAIN_BATCH_SIZE hard-coded in hyperparams.json
    with open("hyperparams.json") as fd:
        HYPERPARAMS = json.load(fd)
    mem_key = "memory" if use_memory else "no_memory"
    comp_key = "competitive" if is_competitive else "cooperative"
    return (
        HYPERPARAMS[algo]["no_params"]
        if not use_hyper
        else HYPERPARAMS[algo][comp_key][mem_key]
    )


def generate_training_config(
    env: "AntiPoaching",
    env_config: dict,
    policies_to_train: list = None,
    hyperparams: dict = None,
    config_class: AlgorithmConfig = PPOConfig,
    policy_class_selector: Callable = None,
    num_gpus: int = 0,
) -> AlgorithmConfig:
    """Generate a training-only AlgorithmConfig that can be used by Tune"""

    if env_config is None:
        env_config = {}
    if policies_to_train is None:
        policies_to_train = env.agents  # train everyone by default
    if hyperparams is None:
        hyperparams = {}
    if policy_class_selector is None:
        # None PolicyClass -> RLlib automatically infers
        policy_class_selector = lambda agent: None

    return (
        config_class()
        .environment(
            anti_poaching.metadata["name"],
            env_config=env_config,
            # Required since env_checking samples random actions,
            # and samples the entire space for each agent. This is
            # not desired. !!!
            disable_env_checking=True,
        )
        # .experimental(_disable_preprocessor_api=True)
        .framework("torch")
        .training(
            model={
                "custom_model": "TorchActionMaskModel",
                "custom_model_config": {
                    "no_masking": False
                },  # Required by the example
            },
        )
        .resources(num_gpus=num_gpus)
        .multi_agent(
            policies={
                agent: PolicySpec(
                    policy_class_selector(agent),
                    observation_space=env.observation_space(agent),
                    action_space=env.action_space(agent),
                )
                for agent in env.agents
            },
            policy_mapping_fn=lambda aid, episode, worker, **kw: aid,
            policies_to_train=policies_to_train,
        )  # end-multi-agent
        .update_from_dict(hyperparams)
    )


def ppo_config(
    env: "AntiPoaching",
    env_config: dict = None,
    policies_to_train=None,
    num_gpus: int = 0,
    is_competitive: bool = False,
    use_memory: bool = False,
    use_hyper: bool = False,
    policy_class_selector: Callable = None,
) -> PPOConfig:
    """Builds the default PPO Config for AntiPoachingGame"""
    _hyperparams = read_hyperparams_for_algo(
        "PPO",
        is_competitive=is_competitive,
        use_memory=use_memory,
        use_hyper=use_hyper,
    )
    return (
        generate_training_config(
            env=env,
            env_config=env_config,
            policies_to_train=policies_to_train,
            hyperparams=_hyperparams,
            config_class=PPOConfig,
            policy_class_selector=policy_class_selector,
            num_gpus=num_gpus,
        )
        .rl_module(_enable_rl_module_api=False)
        .rollouts(rollout_fragment_length="auto")
    )


def pg_config(
    env: "AntiPoaching",
    env_config: dict = None,
    policies_to_train=None,
    num_gpus: int = 0,
    is_competitive: bool = False,
    use_memory: bool = False,
    use_hyper: bool = False,
    policy_class_selector: Callable = None,
) -> PGConfig:
    """Builds the default PG Config for AntiPoachingGame"""
    _hyperparams = read_hyperparams_for_algo(
        "PG",
        is_competitive=is_competitive,
        use_memory=use_memory,
        use_hyper=use_hyper,
    )
    return generate_training_config(
        env=env,
        env_config=env_config,
        policies_to_train=policies_to_train,
        hyperparams=_hyperparams,
        config_class=PGConfig,
        policy_class_selector=policy_class_selector,
        num_gpus=num_gpus,
    )


def qmix_config(
    env: GroupAgentsWrapper,
    env_config: dict = None,
    policies_to_train=None,
    num_gpus: int = 0,
    is_competitive: bool = False,
    use_memory: bool = False,
    use_hyper: bool = False,
    policy_class_selector: Callable = None,
) -> QMixConfig:
    assert isinstance(
        policies_to_train, list
    ), f"{policies_to_train=} is not a list."
    if env_config is None:
        env_config = {}
    if policies_to_train is None:
        policies_to_train = ["rangers"]
    if policy_class_selector is None:
        # None PolicyClass -> RLlib automatically infers
        policy_class_selector = lambda agent: None
    _hyperparams = read_hyperparams_for_algo(
        "QMIX",
        is_competitive=is_competitive,
        use_memory=use_memory,
        use_hyper=use_hyper,
    )

    env = grouped_rllib_cons_game(env, env_config)  # Group the env
    return (
        QMixConfig()
        .framework("torch")
        .environment(
            anti_poaching.metadata["name"] + "_grouped",
            env_config=env_config,
            disable_env_checking=True,
        )
        .resources(num_gpus=num_gpus)
        .multi_agent(
            policies={
                _group: PolicySpec(
                    policy_class_selector(_group),
                    observation_space=env.observation_space[_idx],
                    action_space=env.action_space[_idx],
                )
                for _idx, _group in enumerate(["rangers", "poachers"])
            },
            policy_mapping_fn=lambda aid, episode, worker, **kwargs: (
                "rangers" if "ranger" in aid else "poachers"
            ),
            policies_to_train=policies_to_train,
        )  # end-multi-agent
        .update_from_dict(_hyperparams)
    )


def duplicator(base_config: AlgorithmConfig) -> AlgorithmConfig:
    """Returns a function object that just duplicates the base
    config, but obeys the calling convention"""
    return lambda base_config=base_config, *args, **kwargs: base_config.copy()


def generic_conf(config_class: AlgorithmConfig) -> AlgorithmConfig:
    """Generates a function that can create a general configuration
    but with a specific class. Useful for quickly adding algorithms"""
    return functools.partial(
        generate_training_config, config_class=config_class
    )
