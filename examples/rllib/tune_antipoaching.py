"""
Module to implement the hyperparameter tuning for all supported algorithms.
This currently includes PPO, PG and QMIX. This module launches a parameter 
search  over the default 2R2P version of the game for one algo. We use Ray Tune
to automatically launch this search.

Currently, this supports the following arguments to configure the hyperparameter
search. 
 
 .. highlight:: bash
 .. code-block:: bash
 
     --num-samples     Number of trials to launch with random paramters for the search.
     --stop-iters      Number of iterations to train.
     --stop-timesteps  Number of timesteps to train each trial.
     --stop-reward     If the average reward attains this value, the trial stops.
     --competitive     Measure for competitive experiments i.e. agents minimise
                        the exploitability metric. False by default.
     --memory          Whether to use the LSTM models defined in `recurrent.py`.
                        False by default.

These options are in addition to the ones already supported by `get_args` and
`define_game_from_args`.
"""

# Core modules
import json
import argparse
import pathlib
import functools

# Ray imports
import numpy as np
import ray
import torch
from ray import air, tune
from ray.rllib.algorithms.ppo import PPO, PPOConfig
from ray.rllib.algorithms.pg import PG, PGConfig
from ray.rllib.algorithms.qmix import QMixConfig, QMix

# Importing custom code
from anti_poaching.anti_poaching_v0 import anti_poaching
from heuristic_policies import (
    ActionMaskedRandomPolicy,
    ActionMaskedStaticPoacherPolicy,
    QMIXActionMaskedRandomPolicy,
    QMIXGreedyStaticPolicy,
)

import callbacks as cb
from configs import (
    ppo_config,
    pg_config,
    qmix_config,
    duplicator,
    _TRAIN_BATCH_SIZE,
)
from example_utils import get_args, define_game_from_args, trial_path_generator
from recurrent import LSTMActionMaskingModel, QmixLSTMModel
from main import get_policies_from_arg

COMMON_HYPERPARAMS = {
    "train_batch_size": tune.choice([128, 256, 512, 1024, 2048, 4096]),
    "gamma": tune.choice([0.9, 0.95, 0.97, 0.99, 0.999]),
    "lr": tune.choice([3e-05, 0.0001, 0.0004, 0.001]),
}
MEMORY_HYPERPARAMS = {
    "custom_model_config": {"no_masking": False},
    "lstm_use_prev_action": tune.choice([True, False]),
    "lstm_use_prev_reward": tune.choice([True, False]),
}
PG_HYPERPARAMS = {**COMMON_HYPERPARAMS}
PPO_HYPERPARAMS = {
    **COMMON_HYPERPARAMS,
    "num_sgd_iter": tune.choice(
        [10, 20, 30]
    ),  # Number of SGD iterations in each outer loop (n_update_epochs),
    "sgd_minibatch_size": tune.sample_from(
        lambda spec: random.choice(
            [x for x in [64, 128, 256] if x <= spec.config.train_batch_size]
        )
    ),
    "clip_param": tune.choice([0.1, 0.2, 0.25, 0.3, 0.4, 0.5]),
    "grad_clip": tune.choice([None, 0.5, 1.0, 2.0, 5.0]),
    "use_gae": tune.choice([True, False]),
    "use_critic": tune.choice([True, False]),
    "lambda_": tune.choice([0.9, 0.95, 0.99]),
    "vf_loss_coeff": tune.choice([0.001, 0.01, 0.1, 0.5, 1.0]),
}
QMIX_HYPERPARAMS = {
    **COMMON_HYPERPARAMS,
    "mixer": tune.choice(["qmix", "vdn"]),
    "grad_clip": tune.choice([None, 0.5, 1.0, 2.0, 5.0, 10.0]),
    "target_network_update_freq": tune.choice([500, 1000, 2000, 5000]),
}

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--stop-iters", type=int, help="Number of iterations to train."
    )
    parser.add_argument(
        "--stop-timesteps", type=int, help="Number of timesteps to train."
    )
    parser.add_argument(
        "--stop-reward", type=float, help="Reward at which we stop training."
    )
    parser.add_argument(
        "--competitive",
        action="store_true",
        default=False,
        help="""Measure for competitive experiements (all agents train to minimise exploitability), 
                or cooperative (only rangers train to maximise total mean reward. (Default: cooperative)""",
    )
    parser.add_argument(
        "--memory",
        action="store_true",
        default=False,
        help="Use LSTM/Memory paramters for search as well",
    )
    parser.add_argument(
        "--num-samples",
        type=int,
        default=5,
        help="""Number of times to sample from the hyperparameter space.
                If this is -1, (virtually) infinite samples are generated until a stopping condition is met.""",
    )

    # First parsing known args for AntiPoaching game
    # Then parsing the above args for the tune search
    cg_args, _unknown = get_args()
    cg, cg_config = define_game_from_args(cg_args)
    args, _ = parser.parse_known_args()

    ray.init(
        configure_logging=True,
        log_to_driver=cg_args.verbose,
        num_gpus=np.ceil(cg_args.num_gpus),
        num_cpus=cg_args.num_cpus,
    )

    # Update stopping criteria
    stop = {}
    if args.stop_iters is not None:
        stop["training_iteration"] = args.stop_iters
    if args.stop_timesteps is not None:
        stop["timesteps_total"] = args.stop_timesteps
    if args.stop_reward is not None:
        stop["episode_reward_mean"] = args.stop_reward

    # Choose the agents that learn
    policies_to_train = []
    if "r" in cg_args.policies_train:
        policies_to_train += (
            ["rangers"] if cg_args.algo == "QMIX" else cg.rangers
        )
    if "p" in cg_args.policies_train:
        policies_to_train += (
            ["poachers"] if cg_args.algo == "QMIX" else cg.poachers
        )

    # Choose the algo that runs
    if cg_args.algo == "PPO":
        algo_config, hyperparams = ppo_config, PPO_HYPERPARAMS
    elif cg_args.algo == "PG":
        algo_config, hyperparams = pg_config, PG_HYPERPARAMS
    elif cg_args.algo == "QMIX":
        algo_config, hyperparams = qmix_config, QMIX_HYPERPARAMS
    else:
        raise RuntimeError(f"Unknown algorithm specified: {args.algo}")

    # Choose the algo that runs
    if cg_args.algo == "PPO":
        algo_config = ppo_config
    elif cg_args.algo == "PG":
        algo_config = pg_config
    elif cg_args.algo == "QMIX":
        algo_config = qmix_config
    else:
        raise RuntimeError(f"Unknown algorithm specified: {cg_args.algo}")

    # Agent specific policies
    policy_class_selector = lambda agent: (
        get_policies_from_arg(cg_args.rpol, cg_args.algo)
        if "ranger" in agent
        else get_policies_from_arg(cg_args.ppol, cg_args.algo)
    )

    # build and run chosen algo
    config = (
        algo_config(
            env=cg,
            env_config=cg_config,
            policies_to_train=policies_to_train,
            policy_class_selector=policy_class_selector,
            use_hyper=cg_args.use_hyper,
            num_gpus=cg_args.num_gpus,  # AlgoConfig to use GPUs.
        )
        .resources(
            num_cpus_per_worker=cg_args.num_cpus_per_worker,
            num_gpus=cg_args.num_gpus,
        )
        .reporting(keep_per_episode_custom_metrics=True)
        .rollouts(
            rollout_fragment_length="auto",
            batch_mode=tune.sample_from(
                lambda spec: (
                    "truncate_episodes"
                    if getattr(spec.config, "use_gae", False)
                    else "complete_episodes"
                )
            ),
        )
    )

    # Using custom reporters and tune configs to add new metrics
    tune_config_params = {
        "reuse_actors": False,
        "trial_name_creator": lambda trial: (
            cg_args.trial_name if cg_args.trial_name else None
        ),
        "num_samples": args.num_samples,
    }

    if args.competitive:
        old_config = config.copy()
        config.callbacks(
            lambda: cb.ExploitabilityCallback(
                env_instance=cg,
                env_config=cg_config,
                new_config_generator=duplicator(old_config),
                _expl_num_iters=1,
            )
        ).evaluation(
            evaluation_num_workers=cg_args.eval_workers,
            evaluation_interval=max(
                1, cg_args.eval_every // old_config.get("train_batch_size")
            ),
            evaluation_duration=100,
            always_attach_evaluation_results=True,
            evaluation_parallel_to_training=True,
        )
        tune_config_params.update(
            {
                "metric": "info/learner/exploitability",
                "mode": "min",
                "max_concurrent_trials": 2,
            }
        )
        policies_to_train = (
            ["rangers", "poachers"] if cg_args.algo == "QMIX" else cg.agents
        )  # OVERRIDE: train all agents in competitive.
    else:
        config.callbacks(cb.SumRangerRewardMetric)
        tune_config_params.update(
            {
                "metric": "custom_metrics/rangers_mean_reward_sum",
                "mode": "max",
            }
        )

    # if using the LSTM
    if args.memory:
        config.update_from_dict(
            {
                "model": {
                    "custom_model": (
                        QmixLSTMModel
                        if "QMIX" in cg_args.algo
                        else LSTMActionMaskingModel
                    ),
                    **MEMORY_HYPERPARAMS,
                }
            }
        )
        if "PPO" in cg_args.algo:
            config["shuffle_sequences"] = False

    # Finally, overwrite with the common choices to optimise.
    config.update_from_dict(COMMON_HYPERPARAMS)

    # using tune to automatically checkpoint
    # to the local directory "results/"
    result_grid = tune.Tuner(
        cg_args.algo,
        param_space=config,
        tune_config=tune.TuneConfig(**tune_config_params),
        run_config=air.RunConfig(
            stop=stop,
            storage_path=pathlib.Path(
                f".results/{trial_path_generator(cg_args.algo)}/"
            ).resolve(),
            checkpoint_config=air.CheckpointConfig(
                checkpoint_at_end=True,
            ),
            failure_config=ray.train.FailureConfig(max_failures=3),
        ),
    ).fit()

    # Create the file to store the results, and write to it.
    if args.memory is True:
        output_file = pathlib.Path(
            f"./.results/tuning_results_{cg_args.algo}_memory.txt"
        )
    else:
        output_file = pathlib.Path(
            f"./.results/tuning_results_{cg_args.algo}.txt"
        )
    output_file.parent.mkdir(exist_ok=True, parents=True)
    with open(output_file.as_posix(), "w+") as fd:
        best_trial = result_grid._experiment_analysis.get_best_trial()
        best_result = result_grid.get_best_result()
        best_config = best_trial.config

        fd.write(f"best result: {best_result}\n")
        fd.write(f"best_trial.config: {best_trial.config}\n")
        fd.write(
            f"best_trial.evaluated_params: {best_trial.evaluated_params}\n"
        )

    # Shutdown gracefully, saving if required.
    ray.shutdown()
