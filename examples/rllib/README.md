# RLlib Runner


[main.py](./main.py) demonstrates the integration with RLlib (ray 2.8.0). However, we divide the code into multiple files based on their purpose.

- [callbacks.py](./callbacks.py) implements metric collection callbacks that can be optionally enabled, such as the Exploitability measure.
- [configs.py](./configs.py) contains functions that create standard AlgorithmConfig objects, as required for the Anti-Poaching Game. Currently, this includes configurations for PPO, Policy Gradients and QMIX algorithms.
- [example_utils.py](./example_utils.py) defines the argument parser and `define_game_from_args`, which creates an appropriate Anti-Poaching game instance, based on a given `ArgumentParser` object.
- [heuristic_policies.py](./heuristic_policies.py) implements the custom heuristic policies that an agent can follow. These policies do not learn.


## main.py

The main module implements PPO/PG/QMIX for the AntiPoachingGame and optionally measures custom metrics. The custom callbacks also include callbacks for before and after calls to Algorithm.evaluate(); Further customisation points can be created in the [callbacks.py](callbacks.py) file. The `main.py` as is permits us to change various game parameters, the algorithm used to run the game, the policies used by the agents, the training agent set, and some other execution specs like the number of CPUs used.


### Examples for main.py
Consider the following example between 4 Rangers and 5 Poachers (4Rv5P). We want train only the Rangers using QMIX over 30k timesteps, while the Poachers follow a Greedy/Planning heuristic strategy. In addition, we would like to evaluate the learned policies every 10k steps. The following command covers this case.
```bash
$ python main.py --no-expl --policies-train r --ppol greedy \ 
             --num-cpus 12 --eval-every 10000 --timesteps 30000 \
             --algo QMIX --rangers 4 --poachers 5 --grid 20
```

To list all available options, run

```bash
$ python main.py --help
```

Note that `main.py` will also save an `env_config.json` file in the experiment directory, which is the exact environment configuration using during an experiment. This is needed since RLlib does not save it in the checkpoint.

## Visualisation

To run an episode visually using some pre-trained policies (note that this must be a full algorithm checkpoint for now), we use the [from_checkpoint.py](from_checkpoint.py) file as follows.
```bash
$ python from_checkpoint.py <PATH_TO_CHKPOINT_DIR>
```

If these policies were generated using [main.py](main.py), these will be saved in the local directory `.results/`. Note that `from_checkpoint.py` also requires the `env_config.json` file to run the experiment; it is therefore suggested to use `main.py` since it will automatically generate this for you.

Lastly, note that you must supply a filepath of the form ".results/QMIX_<DATE>/<EXPERIMENT-NAME>/checkpoint_000000", i.e. the path to the checkpoint directory, and not to the `rllib_checkpoint.json` file.
