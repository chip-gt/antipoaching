#/bin/bash

script_path="$(realpath ./generate_plots.py)";
cur_path=$(pwd)
if [ -z "$1" ]
  then
    dir_path=$(realpath ./)
else
    dir_path=$1
fi

cd $dir_path;

generate_subfolder_plots(){
    echo BASH: $pwd
    for subfolder in $(find $1 -maxdepth 1 ! -path . ! -path 'plots_*' -type d); do 
      echo "File: $subfolder"; 
      python $script_path --logdir $(realpath $subfolder) &
    done;
    wait;
    cd ../; 
}

export -f generate_subfolder_plots;
export script_path;

parallel -j6 generate_subfolder_plots {} ::: $(ls -d [P,Q]*);

cd ${cur_path};
