import argparse
import pandas as pd
import os.path
import os
import json
import sys
from matplotlib import pyplot as plt
import seaborn as sns
import numpy as np
from io import BytesIO
from concurrent import futures
from ray.tune.utils.util import flatten_dict


def encode_fig_pdf(plt, name):
    figfile = BytesIO()
    plt.savefig(figfile, format="pdf", bbox_inches="tight", transparent=True)
    figfile.seek(0)  # rewind to beginning of file
    return (name.replace(".", "_") + ".pdf", figfile.read())


def add_line(str, level=0):
    return (" " * level) + str + "\n"


def generate_report(pdf_plots, params, path, num_samples):
    latex = ""
    latex += add_line("\documentclass{article}")
    latex += add_line("\\usepackage{graphicx}")
    latex += add_line("\\begin{document}")
    latex += add_line("\section{Experiment}")
    for key in params.keys():
        latex += add_line("\\begin{table}")
        latex += add_line("\\begin{tabular}{|l|c|}")
        latex += add_line("\hline")
        for key2 in params[key].keys():
            latex += add_line(
                key2.replace("_", "\_")
                + " & "
                + str(params[key][key2]).replace("_", "\_")
                + "\\\\"
            )
        latex += add_line("\hline")
        latex += add_line("\end{tabular}")
        latex += add_line(
            "\caption{Hyperparameters used in configuration "
            + key.replace("_", "\_")
            + " }"
        )
        latex += add_line("\end{table}")
    latex += add_line(
        "For each of the sampled choice configurations, we train %d agents with different random seeds and compute the performance metrics."
        % num_samples
    )
    latex += add_line("\section{Training plots}")
    latex += add_line(
        "Plots that consider training iterations.\\footnote{A training iteration includes parallel sample collection by the environment workers as well as loss calculation on the collected batch and a model update.}"
    )
    latex += add_line("\\begin{figure}[ht]")
    latex += add_line("\\begin{center}")
    latex += add_line(
        "\centerline{\includegraphics[width=1\\textwidth]{training_policies_reward.pdf}}"
    )
    latex += add_line(
        "\caption{Training curves. Mean over different runs and 95\% confidence intervals bands. \label{fig:training_curves}}"
    )
    latex += add_line("\end{center}")
    latex += add_line("\end{figure}")
    latex += add_line(
        " Figure~\\ref{fig:training_curves} plots the total discounted reward for each complete episode explored during training for each policy and for the sum of them (episode). Each line represents the mean over different runs (with different samples) and the shaded regions show the 95\% confidence intervals bands.\\\\"
    )
    latex += add_line("\\begin{figure}[ht]")
    latex += add_line("\\begin{center}")
    latex += add_line(
        "\centerline{\includegraphics[width=1\\textwidth]{training_iteration_mean_policy_loss.pdf}}"
    )
    latex += add_line("\caption{Training policy loss.}")
    latex += add_line("\label{fig:training_mean_policy_loss}")
    latex += add_line("\end{center}")
    latex += add_line("\end{figure}")
    latex += add_line(
        " Figure~\\ref{fig:training_mean_policy_loss} plots the mean magnitude of policy loss function for each training iteration. This measure correlates to how much the policy (process for deciding actions) is changing. The magnitude of this should decrease during a successful training process. These values will oscilate during training. Generally they should be less than 1.\\\\"
    )

    latex += add_line("\\begin{figure}[ht]")
    latex += add_line("\\begin{center}")
    latex += add_line(
        "\centerline{\includegraphics[width=1\\textwidth]{training_iteration_value_function_explained_variance.pdf}}"
    )
    latex += add_line("\caption{Training value function explained variance.}")
    latex += add_line(
        "\label{fig:training_iteration_value_function_explained_variance}"
    )
    latex += add_line("\end{center}")
    latex += add_line("\end{figure}")
    latex += add_line(
        " Figure~\\ref{fig:training_iteration_value_function_explained_variance} plots the explained variance (ev) of the value function for each training iteration. This measure calculates if the value function is a good predicator of the returns ($ev>1$) or if it is worse than predicting nothing ($ev=<0$). $ev = (1- Var[empirical return - predicted value])/Var[empirical return]$.\\\\"
    )

    latex += add_line("\\begin{figure}[ht]")
    latex += add_line("\\begin{center}")
    latex += add_line(
        "\centerline{\includegraphics[width=1\\textwidth]{training_iteration_entropy.pdf}}"
    )
    latex += add_line("\caption{Training entropy.}")
    latex += add_line("\label{fig:training_iteration_entropy}")
    latex += add_line("\end{center}")
    latex += add_line("\end{figure}")
    latex += add_line(
        " Figure~\\ref{fig:training_iteration_entropy} plots the  entropy for each training iteration. The entropy measures how random the decision of the model are. Entropy should slowly decrease during a successful training process. However, if entropy decreases too quickly, this means that policy becomes deterministic and there is no learning. If entropy does not decrease at all there is also no learning (the policy is just a random policy).\\\\"
    )

    latex += add_line("\section{Evaluation plots}")
    latex += add_line("Plots that consider evaluation iterations.")
    latex += add_line("\\begin{figure}[ht]")
    latex += add_line("\\begin{center}")
    latex += add_line(
        "\centerline{\includegraphics[width=1\\textwidth]{evaluation_policies_mean_reward.pdf}}"
    )
    latex += add_line("\caption{Evaluation mean reward par episode.}")
    latex += add_line("\label{fig:evaluation_episodes_mean_reward}")
    latex += add_line("\end{center}")
    latex += add_line("\end{figure}")
    latex += add_line(
        " Figure~\\ref{fig:evaluation_episodes_mean_reward} plots the reward for each episode explored during evaluation iterations.\\\\"
    )
    latex += add_line("\\begin{figure}[ht]")
    latex += add_line("\\begin{center}")
    latex += add_line(
        "\centerline{\includegraphics[width=1\\textwidth]{evaluation_policies_mean_reward_vs_training_experiences.pdf}}"
    )
    latex += add_line(
        "\caption{Evaluation mean reward par episode with respect to the number of training experiences used.}"
    )
    latex += add_line(
        "\label{fig:evaluation_episodes_mean_reward_vs_training_experiences}"
    )
    latex += add_line("\end{center}")
    latex += add_line("\end{figure}")
    latex += add_line(
        " Figure~\\ref{fig:evaluation_episodes_mean_reward_vs_training_experiences} plots the reward for each episode explored during evaluation in relation to how many training experiences have been used.\\\\"
    )
    latex += add_line("\\begin{figure}[ht]")
    latex += add_line("\\begin{center}")
    latex += add_line(
        "\centerline{\includegraphics[width=1\\textwidth]{evaluation_policies_exploitability.pdf}}"
    )
    latex += add_line("\caption{Evaluation exploitability par evaluation.}")
    latex += add_line("\label{fig:evaluation_episodes_exploitability}")
    latex += add_line("\end{center}")
    latex += add_line("\end{figure}")
    latex += add_line(
        " Figure~\\ref{fig:evaluation_episodes_exploitability} plots the exploitability of each policy for each episode explored during evaluation iterations.\\\\"
    )
    latex += add_line("\end{document}")
    pdf_plots.append(("main.tex", latex))
    publish_report(pdf_plots, path)


def publish_report(pdf_plots, path):
    os.makedirs(path, exist_ok=True)

    def save_file(inp):
        filename, file = inp
        path2 = path + "/" + filename
        with open(path2, "w" + ("b" if ".pdf" in filename else "")) as f:
            f.write(file)
        print("%s available at %s" % (filename, path2))

    with futures.ThreadPoolExecutor(max_workers=100) as executor:
        executor.map(save_file, pdf_plots)


# Plots some metric by policy (y-axis) vs training iteration (x-axis)
def training_iteration_metric_plot(
    metric, metric_title, graph_title, all_results
):
    dfi = []
    df = pd.DataFrame(
        columns=[
            "Training iteration",
            metric_title,
            "Configuration",
            "Policy",
        ]
    )
    for conf, conf_results in all_results.items():
        for key, results in conf_results.items():
            counter = 0
            for ind in results.index:
                for policy in results["info"][ind]["learner"].keys():
                    # Get the metric title, or skip if keys not found
                    _row_results = results["info"][ind]["learner"][policy]
                    _result_keys = getattr(_row_results, "keys", None)
                    if not _result_keys:
                        continue

                    # Now, get row_metric to fill column
                    row_metric = None
                    if (
                        "learner_stats" in _result_keys()
                        and metric in _row_results["learner_stats"]
                    ):
                        row_metric = _row_results["learner_stats"][metric]
                    elif metric in _row_results.keys():
                        row_metric = _row_results[metric]

                    # Define the new row and append it to df
                    dfi.append(
                        {
                            "Training iteration": counter,
                            metric_title: row_metric,
                            "Configuration": conf,
                            "Policy": policy,
                        }
                    )
                counter += 1

    df = pd.concat([df, pd.DataFrame.from_dict(dfi)], ignore_index=True)
    plt.figure(figsize=(16, 6))
    # Always MARL
    sns.lineplot(
        data=df,
        x="Training iteration",
        y=metric_title,
        hue="Policy",
        style="Configuration",
    )
    pdf = encode_fig_pdf(plt, graph_title)
    plt.close()
    return pdf


def training_policies_reward_plot(all_results):
    df = pd.DataFrame(
        columns=[
            "Episode",
            "Total discounted reward",
            "Configuration",
            "Policy",
        ]
    )
    for conf in all_results.keys():
        conf_results = all_results[conf]
        for key in conf_results.keys():
            results = conf_results[key]
            counter = 0
            for ind in results.index:
                n_episodes_iter = results["episodes_this_iter"][ind]
                for policy in results["sampler_results"][ind][
                    "hist_stats"
                ].keys():
                    if policy.find("_reward") != -1:
                        assert (
                            len(
                                results["sampler_results"][ind]["hist_stats"][
                                    policy
                                ]
                            )
                            >= n_episodes_iter
                        ), f"Found number of episodes rewards in training iterations lower than the number of episodes in the iteration {ind}"
                        # This can happen because rllib adds older historical
                        # episodes in order to reach the required smooting
                        # window "metrics_num_episodes_for_smoothing". I saw in
                        # the code that these episodes are added at the beginning of the list
                        diff_n_episodes = (
                            len(
                                results["sampler_results"][ind]["hist_stats"][
                                    policy
                                ]
                            )
                            - n_episodes_iter
                        )
                        for iter_counter in range(0, n_episodes_iter):
                            new_row = {
                                "Episode": iter_counter + counter,
                                "Total discounted reward": results[
                                    "sampler_results"
                                ][ind]["hist_stats"][policy][
                                    iter_counter + diff_n_episodes
                                ],
                                "Configuration": conf,
                                "Policy": policy,
                            }
                            df.loc[len(df)] = new_row
                counter += n_episodes_iter
    plt.figure(figsize=(16, 6))
    if df["Policy"].nunique() == 1:  # single-RL
        sns.lineplot(
            data=df,
            x="Episode",
            y="Total discounted reward",
            hue="Configuration",
            style="Configuration",
        ).set_title("Episode Rewards during training")
    else:  # marl
        sns.lineplot(
            data=df,
            x="Episode",
            y="Total discounted reward",
            hue="Policy",
            style="Configuration",
        ).set_title("Episode Rewards during training")
    pdf = encode_fig_pdf(plt, "training_policies_reward")
    plt.close()
    return pdf


def evaluation_policies_mean_reward(all_results):
    # dfs = []
    df = pd.DataFrame(
        columns=[
            "Evaluation iteration",
            "Number of experiences",
            "Policy Episode Reward",
            "Policy",
            "Configuration",
        ]
    )
    max_evaluation_iteration = 0
    for conf in all_results.keys():
        conf_results = all_results[conf]

        for key in conf_results.keys():
            results = conf_results[key]
            # Plot the reward for the evaluation graph
            # We get the evaluation interval to make sure that we only get
            # evaluation data in iterations for which we performed evaluation
            evaluation_interval = results["config"][0]["evaluation_interval"]
            # We get also the train_batch_size
            train_batch_size = results["config"][0]["train_batch_size"]
            i = 1
            evaluation_iteration = 1
            if "evaluation" in results.keys():
                for ind in results["evaluation"].index:
                    if i == evaluation_interval:
                        i = 1
                        for policy in results["evaluation"][ind][
                            "hist_stats"
                        ].keys():
                            if policy.find("_reward") != -1:
                                dfi = pd.DataFrame(
                                    {
                                        "Evaluation iteration": np.full(
                                            results["evaluation"][ind][
                                                "episodes_this_iter"
                                            ],
                                            evaluation_iteration,
                                        ),
                                        "Number of experiences": np.full(
                                            results["evaluation"][ind][
                                                "episodes_this_iter"
                                            ],
                                            train_batch_size
                                            * evaluation_iteration,
                                        ),
                                        "Policy Episode Reward": results[
                                            "evaluation"
                                        ][ind]["hist_stats"][policy],
                                        "Policy": policy,
                                        "Configuration": conf,
                                    }
                                )
                                df = pd.concat([df, dfi], ignore_index=True)
                        evaluation_iteration = evaluation_iteration + 1
                    else:
                        i = i + 1
            max_evaluation_iteration = max(
                max_evaluation_iteration, evaluation_iteration
            )
    plt.figure(figsize=(16, 6))
    if df["Policy"].nunique() == 1:  # single-RL
        sns.lineplot(
            data=df,
            x="Evaluation iteration",
            y="Policy Episode Reward",
            hue="Configuration",
            style="Configuration",
        ).set_title("Policy mean episode rewards par evaluation iteration")
    else:  # marl
        sns.lineplot(
            data=df,
            x="Evaluation iteration",
            y="Policy Episode Reward",
            hue="Policy",
            style="Configuration",
        ).set_title("Policy mean episode rewards par evaluation iteration")
    # plt.xticks(range(max_evaluation_iteration))
    pdf = encode_fig_pdf(plt, "evaluation_policies_mean_reward")
    plt.close()
    plt.figure(figsize=(16, 6))
    if df["Policy"].nunique() == 1:  # single-RL
        sns.lineplot(
            data=df,
            x="Number of experiences",
            y="Policy Episode Reward",
            hue="Configuration",
            style="Configuration",
        ).set_title("Policy mean episode rewards vs training  experiences")
    else:  # marl
        sns.lineplot(
            data=df,
            x="Number of experiences",
            y="Policy Episode Reward",
            hue="Policy",
            style="Configuration",
        ).set_title("Policy mean episode rewards vs training experiences")
    pdf_experiences = encode_fig_pdf(
        plt, "evaluation_policies_mean_reward_vs_training_experiences"
    )
    plt.close()
    return [pdf, pdf_experiences]


def evaluation_exploitability(all_results):
    dfi, max_evaluation_iteration = [], 0
    df_columns = [
        "Evaluation iteration",
        "Policy exploitability",
        "Policy",
        "Configuration",
    ]
    df = pd.DataFrame(columns=df_columns)
    for conf, conf_results in all_results.items():
        for key, results in conf_results.items():
            # Plot the reward for the evaluation graph
            # We get the evaluation interval to make sure that we only get
            # evaluation data in iterations for which we performed evaluation
            i, evaluation_iteration = 1, 1
            evaluation_interval = results["config"][0]["evaluation_interval"]
            if "evaluation" not in results.keys():
                continue
            expl_results = results["info"]
            for ind in expl_results.index:
                # If not an eval iteration, continue
                if i != evaluation_interval:
                    i = i + 1
                    continue

                # else, reset the evaluation_iteration count
                # and check if exploitability data was stored.
                i = 1
                if not "policy_exploitability" in expl_results[ind]["learner"]:
                    continue  # Skip, exploitability not here...

                # Now processing exploitability data ...
                for policy in expl_results[ind]["learner"][
                    "policy_exploitability"
                ].keys():
                    dfi.append(
                        {
                            "Evaluation iteration": results[
                                "num_env_steps_sampled"
                            ][ind],
                            "Policy exploitability": expl_results[ind][
                                "learner"
                            ]["policy_exploitability"][policy],
                            "Policy": policy,
                            "Configuration": conf,
                        }
                    )
                evaluation_iteration = evaluation_iteration + 1
            max_evaluation_iteration = max(
                max_evaluation_iteration, evaluation_iteration
            )

    df = pd.concat([df, pd.DataFrame.from_dict(dfi)])
    plt.figure(figsize=(16, 6))
    sns.lineplot(
        data=df,
        x="Evaluation iteration",
        y="Policy exploitability",
        hue="Policy",
        style="Configuration",
    ).set_title("Policy exploitability par evaluation iteration")
    pdf = encode_fig_pdf(plt, "evaluation_policies_exploitability")
    plt.close()
    return pdf


def get_immediate_subdirectories(path):
    return [f.path for f in os.scandir(path) if f.is_dir()]


def load_experiment_parameters(logdir):
    # Check that the directory exists
    if not os.path.isdir(logdir):
        sys.exit("Error directory " + logdir + " does not exist. ")
    # Check if params.json exists in this location
    fname = os.path.abspath(os.path.join(logdir, "params.json"))
    if not os.path.isfile(fname):
        sys.exit("Error file " + fname + " does not exist. ")
    f = open(fname, "r")
    params = flatten_dict(json.loads(f.read()))
    f.close()
    return params


def load_results(logdir, results):
    # Check that the directory exists
    if not os.path.isdir(logdir):
        sys.exit("Error directory " + logdir + " does not exist. ")
    # Check if results.json exists in this location
    fname = os.path.abspath(os.path.join(logdir, "result.json"))
    if os.path.isfile(fname):
        results[logdir] = pd.read_json(fname, lines=True)
        return results
    else:
        for sublogdir in get_immediate_subdirectories(logdir):
            # Call recursively with subfolders
            results = load_results(sublogdir, results)
        return results


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Generates main graphs for scientific plotting from rllib result.json experiment"
    )
    parser.add_argument(
        "--logdir",
        type=str,
        nargs="+",
        help="List of paths (separated by space) to the directory that contains the log results from the experiment",
        required=True,
    )
    args = parser.parse_args()
    logdirs = args.logdir
    all_results = {}
    all_params = {}
    num_samples = -1
    # For each directory
    for logdir in logdirs:
        print("Processing ", logdir)
        # Get all results from different runs under it
        results = load_results(logdir, {})
        # Print identified subfolders and load parameters
        params = {}
        for key in results.keys():
            print(key)
        key = list(results.keys())[0]
        # Load parameters for the experiment
        all_params[logdir] = load_experiment_parameters(
            key
        )  # Assuming all parameters on average equal TODO verify this
        all_results[logdir] = results
        num_samples = len(results)

    # Check that each logdir provided exists
    # for logdir in logdirs:
    # print("Log directory ", logdir, " exists ", os.path.isdir(logdir))
    # fname = os.path.abspath(os.path.join(logdir, 'result.json'))
    # print("result.json exists ", os.path.isfile(fname))
    # results[logdir] = pd.read_json(fname, lines=True)
    logdir = logdirs[0]

    outputdir = os.path.dirname(logdir)
    outputfolder = "plots"
    for d in logdirs:
        outputfolder = outputfolder + "_" + os.path.basename(d)
    outputdir = os.path.join(outputdir, outputfolder)

    if not (os.path.exists(outputdir)):
        os.makedirs(outputdir)

    pdf_plots = []
    # Generate training reward curve plot
    pdf_plots.append(training_policies_reward_plot(all_results))
    # Generate training policy loss plot
    pdf_plots.append(
        training_iteration_metric_plot(
            "policy_loss",
            "Mean policy loss",
            "training_iteration_mean_policy_loss",
            all_results,
        )
    )
    # Generate explained variance plot
    pdf_plots.append(
        training_iteration_metric_plot(
            "vf_explained_var",
            "Value function explained variance",
            "training_iteration_value_function_explained_variance",
            all_results,
        )
    )
    # Generate entropy plot
    pdf_plots.append(
        training_iteration_metric_plot(
            "entropy", "Entropy", "training_iteration_entropy", all_results
        )
    )
    # Generate evaluation iteration vs mean episode reward curve
    pdf_plots.extend(evaluation_policies_mean_reward(all_results))
    # Generate evaluation iteration vs exploitability
    pdf_plots.append(evaluation_exploitability(all_results))
    generate_report(pdf_plots, all_params, outputdir, num_samples)
    os.chdir(outputdir)
    # run latex compilation twice to get references
    for _ in range(2):
        os.system("yes '' | pdflatex main.tex -interaction=nonstopmode")
