"""Create the graphs from a suitably formatted directory of 
RLlib experiments. 

Use:
    $ python competitive_plots.py [EXPERIMENTS_DIR] [OUTPUT_DIR]

where EXPERIMENTs_DIR will default to the local directory, and start
searching for RLlib experiments. OUTPUT_DIR defaults to ./graphs/ and
will create it if it doesn't exist. 
"""

#!/usr/bin/env python
# coding: utf-8

import sys
import numpy as np
import pandas as pd
import pathlib
import json
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import scipy.stats as st
import scipy

EMPTY_DF = pd.DataFrame(
    {
        "average": [0],
        "ci": [[0, 0]],
        "num_env_steps_sampled": [0],
    }
)

COLORS = {
    "PGnomem": "red",
    "PPOnomem": "blue",
    "QMIXnomem": "green",
    "PGmem": "orange",
    "PPOmem": "cyan",
    "QMIXmem": "olive",
}


def conf_interval(a, confidence=0.9):
    if not isinstance(a, np.ndarray):
        return [0, 0]
    n = len(a)
    m, se = np.mean(a), scipy.stats.sem(a)
    h = se * scipy.stats.t.ppf((1 + confidence) / 2.0, n - 1)
    return m - h, m + h


def mean_eval_ranger_rewards(filepath: pathlib.Path) -> pd.DataFrame:
    # Check if QMIX or not: this affects the average.
    _filepath_str = filepath.as_posix()
    if "QMIX" in _filepath_str:
        _tokens = _filepath_str.split("_")
        idx = _tokens.index("np")
        num_rangers = int(_tokens[idx - 1])

    # Create the dataframe
    with open(filepath, "r") as file:
        l = []
        for line in file:
            line = json.loads(line)
            l.append(line)
        df = pd.json_normalize(l)
    if df.empty:
        return EMPTY_DF

    # Create the mean rewards and the ConfInterval columns
    mean_reward_cols = [
        col
        for col in df.columns
        if "evaluation.policy_reward_mean" in col and "ranger" in col
    ]
    ci_cols = [
        col
        for col in df.columns
        if "evaluation.hist_stats.policy_ranger" in col
    ]

    # If no evaluation was done
    if len(ci_cols) == 0:
        return EMPTY_DF

    # Start processing
    # Get the CI from the evaluation episode rewards
    for col in ci_cols:
        df[col] = df[col].apply(np.array)

    # Get the mean evalauation rewards for the rangers team
    eval_reward_mean = df[mean_reward_cols].dropna()
    if "QMIX" not in _filepath_str:
        eval_reward_mean["average"] = eval_reward_mean[mean_reward_cols].mean(
            axis=1
        )
        df["ci"] = [
            np.average(i, axis=0) for i in zip(*[df[_col] for _col in ci_cols])
        ]
        df["ci"] = df["ci"].apply(conf_interval)
    else:
        # QMIX considers all rangers as one team, so
        # their rewards are all summed i.e. we need to normalise manually.
        eval_reward_mean["average"] = (
            eval_reward_mean[mean_reward_cols] / num_rangers
        )
        df["ci"] = [
            np.average(i, axis=0) for i in zip(*[df[_col] for _col in ci_cols])
        ]
        df["ci"] = df["ci"] / num_rangers
        df["ci"] = df["ci"].apply(conf_interval)

    eval_reward_mean["ci"] = df["ci"].dropna()
    eval_reward_mean["num_env_steps_sampled"] = df[
        "counters.num_env_steps_sampled"
    ].loc[eval_reward_mean.index]

    # return
    return eval_reward_mean


def expl_agents(filepath: pathlib.Path) -> pd.DataFrame:
    # Create the dataframe
    with open(filepath, "r") as file:
        l = []
        for line in file:
            line = json.loads(line)
            l.append(line)
        df = pd.json_normalize(l)
    if df.empty:
        return EMPTY_DF

    # Create the mean rewards and the ConfInterval columns
    expl_cols = [
        col for col in df.columns if "info.learner.exploitability" in col
    ]

    # Get the mean evalauation rewards for the rangers team
    eval_expl = df[expl_cols].dropna()
    eval_expl["num_env_steps_sampled"] = df[
        "counters.num_env_steps_sampled"
    ].loc[eval_expl.index]

    # return
    return eval_expl


def make_title_from_group(group: str) -> str:
    """Makes the title of the plot."""
    tokens = group.split("_")
    _title = f"{tokens[1]}R{tokens[3]}P on {tokens[5]}x{tokens[5]} grid"
    return _title


def plot_group(
    group: str, exps: list, output_dir: pathlib.Path, is_expl: bool = False
) -> None:
    dfs = {}
    fig, ax = plt.gcf(), plt.gca()
    num_rangers, num_poachers = (int(i) for i in group.split("_")[1:4:2])
    for exp in exps:
        # Calculate and extract the dataa
        algo = exp.as_posix().split("/")[-1].split("_")[0]
        if is_expl:
            df = expl_agents(exp / "result.json")
            x, y = (
                df["num_env_steps_sampled"],
                df["info.learner.exploitability"]
                // (num_rangers + num_poachers),
            )
        else:
            df = mean_eval_ranger_rewards(exp / "result.json")
            x, y = df["num_env_steps_sampled"].astype(int), df["average"]
            ci_lower, ci_upper = zip(*df["ci"])

        # plot only 10 points
        stride = int(np.ceil(len(x) / 10))

        # Get experiment name
        exp_tokens = exp.as_posix().split("/")[-1].split("_")
        exp_name = "IL-" + exp_tokens[0]
        algo += exp_tokens[7]
        if exp_tokens[7] == "mem":
            # include RNN status as well
            exp_name += " + RNN"

        plt.plot(
            x[::stride],
            y[::stride],
            "-*",
            label=exp_name,
            color=COLORS[algo],
        )
        if not is_expl:
            # Confidence interval only for average reward.
            plt.fill_between(
                x[::stride],
                ci_lower[::stride],
                ci_upper[::stride],
                color=COLORS[algo],
                alpha=0.2,
            )
            plt.axhline(
                y=100 * num_poachers / num_rangers,
                color="black",
                linestyle="-.",
            )

    # finishing touches
    plt.title(make_title_from_group(group))
    plt.grid()
    ax.xaxis.get_major_formatter().set_scientific(False)
    ax.xaxis.get_major_formatter().set_useOffset(False)

    # Legend: present in sorted order
    handles, labels = ax.get_legend_handles_labels()
    labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
    plt.legend(loc="best")
    plt.xlabel("Sampled Environment Timesteps (SET)")
    if not is_expl:
        plt.ylabel("Average Ranger Reward (ARR)")
        plt.savefig(output_dir / (group + ".png"))
    else:
        plt.ylabel("Approximate Exploitability")
        plt.savefig(output_dir / (group + "_expl.png"))
    plt.clf()


if __name__ == "__main__":
    path = pathlib.Path(sys.argv[1] if len(sys.argv) > 1 else "./")
    output_dir = pathlib.Path(
        sys.argv[2] if len(sys.argv) == 3 else "./graphs"
    ).resolve()

    # Create the output directory for the files.
    output_dir.mkdir(exist_ok=True)

    exp_dirs = []
    for filepath in path.glob("*/*/result.json"):
        if "_mem_" in filepath.as_posix():
            continue
        exp_dirs.append(filepath.parent)

    exp_groups = {}
    for path in exp_dirs:
        group = "_".join(path.as_posix().split("/")[-1].split("_")[1:7])
        algo = path.as_posix().split("/")[-1].split("_")[0]
        if group not in exp_groups:
            exp_groups[group] = [path]
        # elif all(algo not in _path.as_posix() for _path in exp_groups[group]):
        else:
            exp_groups[group].append(path)

    for group, exps in exp_groups.items():
        plot_group(group, exps, output_dir)
        plot_group(group, exps, output_dir, is_expl=True)
