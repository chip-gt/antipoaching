#!/bin/bash
# intialisation script to create the environment and 
# install dependencies as found in requirements.txt
# Author : Prasanna <siva-sri-prasanna@inrae.fr>
# Created: 03 May 2023
# Last Modified: 13 May, 2024

# Name of the virtualenv.
VENV=ape

# Create virtual env
echo "Creating Virtual Environment ..."
virtualenv ${VENV}
source ${VENV}/bin/activate

# Install optional requirements if 
# full/f is supplied as first arg.
if [[ "$1" =~ ^([fF][uU][lL][lL]|[fF])$ ]]
then
    # install rllib only if required.
    echo "Installing RLlib-specific dependencies ..."
    pip install -e .[gpu,code]
else
    # Install requirements
    echo "Installing required dependencies ..."
    pip install -e .[cpu,code]
fi 

# Installing pre-commit hooks for 
# automatic code linting.
echo "Configuring pre-commit hooks ..."
pre-commit install
