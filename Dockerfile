# syntax=docker/dockerfile:1.4

## local build command
# DOCKER_BUILDKIT=1 docker build --tag registry.forgemia.inra.fr/chip-gt/antipoaching:latest --build-arg BUILDKIT_INLINE_CACHE=1 --progress=plain -f Dockerfile "."

## interactive run
# docker run -it registry.forgemia.inra.fr/chip-gt/antipoaching:latest bash

## example commands from CI & readme
# docker run registry.forgemia.inra.fr/chip-gt/antipoaching:latest pylint anti_poaching/ --fail-under=7
# docker run registry.forgemia.inra.fr/chip-gt/antipoaching:latest pytest [test/]
# docker run registry.forgemia.inra.fr/chip-gt/antipoaching:latest bash -c "coverage run --branch -m pytest&&coverage report --fail-under=80"

ARG REGISTRY_DOMAIN="docker.io"
ARG PYTHON_TAG="3.8"

FROM ${REGISTRY_DOMAIN}/python:${PYTHON_TAG}

ARG DEBIAN_FRONTEND=noninteractive

WORKDIR /data

ENV VIRTUAL_ENV=/opt/.venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

RUN pip install -v --upgrade pip setuptools build


COPY . .
RUN pip install --editable .[code,cpu]


ARG REPO_URL="https://forgemia.inra.fr/chip-gt/antipoaching"
ARG BUILD_DATE="unknown"
ARG VERSION="unknown"
ARG COMMIT="unknown"
ARG BASE_DIGEST="unknown"
LABEL \
    org.opencontainers.image.created=$BUILD_DATE \
    org.opencontainers.image.authors="scidyn@inrae.fr" \
    org.opencontainers.image.url="https://miat.inrae.fr/teams/scidyn" \
    org.opencontainers.image.documentation="$REPO_URL/-/blob/$COMMIT/README.md" \
    org.opencontainers.image.source="$REPO_URL" \
    org.opencontainers.image.version="$VERSION" \
    org.opencontainers.image.revision=$COMMIT \
    org.opencontainers.image.vendor="Scidyn Team - INRAE" \
    org.opencontainers.image.licenses="GPL-3.0" \
    org.opencontainers.image.title="antipoaching" \
    org.opencontainers.image.description="Anti-Poaching Game" \
    org.opencontainers.image.base.digest="$BASE_DIGEST" \
    org.opencontainers.image.base.name="docker.io/library/python:${PYTHON_TAG}"
